from random import shuffle

N = 20

print("> add track (-10,0) -> (0,0)")
print("1")
for i in range(0, N+1):
    if i%4 == 0:
        print("> add track (" + str(i//2) + ",0) -> (" + str(i//2+1) + ",0)")
    elif i%4 == 1:
        print("> add track (" + str(i//2+1) + ",0) -> (" + str(i//2+1) + ",1)")
    elif i%4 == 2:
        print("> add track (" + str(i//2) + ",1) -> (" + str(i//2+1) + ",1)")
    else:
        print("> add track (" + str(i//2+1) + ",1) -> (" + str(i//2+1) + ",0)")
    print(i+2)

engine_type = ["diesel", "steam", "electrical"]
size = ["2","3","4","5","6","7","8"]

for i in range(0, 1):
    print("> create engine " + engine_type[i%3] + " Snake " + str(i) + " " + size[i%7] + " true true")
    print("Snake-" + str(i))

for i in range(1, 1+1):
    print("> add train " + str(i) + " Snake-" + str(i-1))
    print(engine_type[(i-1)%3] + " engine Snake-" + str(i-1) + " added to train " + str(i))

"""
positions = list(range(1, 1+1))
shuffle(positions)

for i in range(0, N):
    if positions[i] <= N // 2:
        print("> put train " + str(i+1) + " at (" + str(positions[i]*10-1) + ",0) in direction 1,0")
    else:
        print("> put train " + str(i+1) + " at (" + str(positions[i]*10+1) + ",0) in direction -1,0")
    print("OK")
"""
print("> put train 1 at (0,0) in direction 1,0")
print("OK")

print("> step " + str(N+1))
print("Train 1 at (" + str(N//2+1) + ",0)")
print("> step 1")
print("Crash of train 1")
print("> exit")
