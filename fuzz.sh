while true; do
	rm /tmp/input$$-*
	radamsa -o /tmp/input$$-%n -n 100 ~/Documents/KIT/2019/Prog/final1/*input.txt
	for filename in /tmp/input$$-*; do
		echo "exit" >> $filename
		java -cp out/production/final1/ -Xshare:on edu.kit.informatik.modelrailwaysimulator.Main < $filename
		if [ $? -eq 0 ]; then
			echo OK
		else
			echo $filename
			#notify-send -t 20000 FAIL
			#break 2
			cp $filename fails
		fi
	done
done
