import sys

fuzz = sys.argv[1]

input = open(fuzz + "_input.txt", "r")
output = open(fuzz + "_output.txt", "r")

for line in input.readlines():
	if not line.startswith("#"):
		print(">", line, end="")

for line in output.readlines():
	if line.startswith("Error, "):
		print("<e")
	else:
		print(line, end="")