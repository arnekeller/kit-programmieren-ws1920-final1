add track (1,1) -> (5,1)
add switch (6,1) -> (8,1),(5,1)
add track (10,1) -> (8,1)
add switch (10,-3) -> (10,1),(12,-3)
add track (10,-3) -> (1,-3)
add track (1,-3) -> (1,1)
create engine steam T3 Emma 1 false true
create engine electrical 103 118 4294967300 true true
list engines
delete rolling stock 103-118
create coach passenger 1 true true
create coach passenger 1 true true
list coaches
add train 1 W1
delete train 1
list trains
add train 1 T3-Emma
add train 1 W1
add train 1 W2
list trains
list engines
create train-set 403 145 4 true true
set switch 4 position (10,1)
set switch 2 position (8,1)
put train 1 at (1,1) in direction 2,0
put train 2 at (10,-2) in direction 0,1
step 5
exit
