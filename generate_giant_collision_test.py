from random import shuffle

N = 100

for i in range(0, N+1):
    print("> add track (" + str(i*10) + ",0) -> (" + str((i+1)*10) + ",0)")
    print(i+1)

engine_type = ["diesel", "steam", "electrical"]
size = ["2","3","4","5","6","7","8"]

for i in range(0, N):
    print("> create engine " + engine_type[i%3] + " Lemming " + str(i) + " " + size[i%7] + " true true")
    print("Lemming-" + str(i))

for i in range(1, N+1):
    print("> add train " + str(i) + " Lemming-" + str(i-1))
    print(engine_type[(i-1)%3] + " engine Lemming-" + str(i-1) + " added to train " + str(i))

positions = list(range(1, N+1))
shuffle(positions)

for i in range(0, N):
    if positions[i] <= N // 2:
        print("> put train " + str(i+1) + " at (" + str(positions[i]*10-1) + ",0) in direction 1,0")
    else:
        print("> put train " + str(i+1) + " at (" + str(positions[i]*10+1) + ",0) in direction -1,0")
    print("OK")


print("> step 2")
print("Crash of train " + ",".join(list(map(str, range(1, N+1)))))
print("> exit")
