add track (0,0) -> (0,14)
create engine diesel T3 Sabine 1 false true
add train 1 T3-Sabine
put train 1 at (0,0) in direction 0,-1
step -20
create coach passenger 1 true false
add train 1 W1
put train 1 at (0,0) in direction 0,-1
step -20
exit
