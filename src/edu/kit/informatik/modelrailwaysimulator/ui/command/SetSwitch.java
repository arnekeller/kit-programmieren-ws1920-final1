package edu.kit.informatik.modelrailwaysimulator.ui.command;

import edu.kit.informatik.modelrailwaysimulator.model.ModelRailwaySimulation;
import edu.kit.informatik.modelrailwaysimulator.model.Vector2D;
import edu.kit.informatik.Terminal;
import edu.kit.informatik.modelrailwaysimulator.ui.CommandLine;
import edu.kit.informatik.modelrailwaysimulator.ui.InvalidInputException;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static edu.kit.informatik.modelrailwaysimulator.ui.command.CommandFactory.NUMBER;
import static edu.kit.informatik.modelrailwaysimulator.ui.command.CommandFactory.VECTOR;

/**
 * Command used to specify the position a switch is set to.
 *
 * @author Arne Keller
 * @version 1.0
 */
public class SetSwitch extends Command {
    /**
     * Name of the set switch command.
     */
    public static final String NAME = "set switch";
    private static final Pattern SET_SWITCH_ARGUMENTS
            = Pattern.compile(" (" + NUMBER + ") position \\((" + VECTOR + ")\\)");

    /**
     * Identifier of the switch to configure.
     */
    private int id;
    /**
     * Position to set the switch to.
     */
    private Vector2D point;

    @Override
    public void apply(ModelRailwaySimulation simulation) {
        if (point == null) {
            throw new IllegalStateException("command not initialized");
        }
        if (simulation.setSwitch(id, point)) {
            Terminal.printLine(CommandLine.OK);
        } else {
            Terminal.printError("could not set switch");
        }
    }

    @Override
    public void parse(String input) throws InvalidInputException {
        if (input == null || !input.startsWith(NAME)) {
            throw new InvalidInputException("unknown command");
        }
        final Matcher matcher = SET_SWITCH_ARGUMENTS.matcher(input.substring(NAME.length()));
        if (!matcher.matches()) {
            throw new InvalidInputException("invalid set switch argument syntax");
        }
        id = Integer.parseInt(matcher.group(1));
        point = Vector2D.parse(matcher.group(2));
    }
}
