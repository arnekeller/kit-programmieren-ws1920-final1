package edu.kit.informatik.modelrailwaysimulator.ui.command;

import edu.kit.informatik.modelrailwaysimulator.model.LogicException;
import edu.kit.informatik.modelrailwaysimulator.model.ModelRailwaySimulation;
import edu.kit.informatik.modelrailwaysimulator.ui.InvalidInputException;

/**
 * Command that can be applied to a simulation.
 * Commands are implemented as separate classes for easy modification and expansion.
 *
 * @author Arne Keller
 * @version 1.0
 */
public abstract class Command {
    /**
     * Apply this command to a model railway simulation.
     *
     * @param simulation simulation to use
     * @throws LogicException on illogical user input
     */
    public abstract void apply(ModelRailwaySimulation simulation) throws LogicException;

    /**
     * Parse user input into this command object.
     *
     * @param input one line of user input
     * @throws InvalidInputException if user input is in any way invalid
     */
    public abstract void parse(String input) throws InvalidInputException;
}
