package edu.kit.informatik.modelrailwaysimulator.ui.command;

import edu.kit.informatik.Terminal;
import edu.kit.informatik.modelrailwaysimulator.model.ModelRailwaySimulation;
import edu.kit.informatik.modelrailwaysimulator.ui.InvalidInputException;

import java.util.List;

/**
 * Command used to print a list of tracks and switches.
 *
 * @author Arne Keller
 * @version 1.0
 */
public class ListTracks extends Command {
    /**
     * Name of the list tracks command.
     */
    public static final String NAME = "list tracks";

    @Override
    public void apply(ModelRailwaySimulation simulation) {
        final List<String> tracks = simulation.listTracks();
        if (tracks.isEmpty()) {
            Terminal.printLine("No track exists");
        } else {
            tracks.forEach(Terminal::printLine);
        }
    }

    @Override
    public void parse(String input) throws InvalidInputException {
        if (input == null || !input.startsWith(NAME)) {
            throw new InvalidInputException("unknown command");
        }
        if (input.length() > NAME.length()) {
            throw new InvalidInputException("too many arguments for list tracks");
        }
    }
}
