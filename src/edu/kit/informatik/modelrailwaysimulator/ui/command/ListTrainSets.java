package edu.kit.informatik.modelrailwaysimulator.ui.command;

import edu.kit.informatik.Terminal;
import edu.kit.informatik.modelrailwaysimulator.model.ModelRailwaySimulation;
import edu.kit.informatik.modelrailwaysimulator.ui.InvalidInputException;

import java.util.List;

/**
 * Command used to print a list of train sets on the terminal.
 *
 * @author Arne Keller
 * @version 1.0
 */
public class ListTrainSets extends Command {
    /**
     * Name of the list train-sets command.
     */
    public static final String NAME = "list train-sets";

    @Override
    public void apply(ModelRailwaySimulation simulation) {
        final List<String> trainSets = simulation.listTrainSets();
        if (trainSets.isEmpty()) {
            Terminal.printLine("No train-set exists");
        } else {
            trainSets.forEach(Terminal::printLine);
        }
    }

    @Override
    public void parse(String input) throws InvalidInputException {
        if (input == null || !input.startsWith(NAME)) {
            throw new InvalidInputException("unknown command");
        }
        if (input.length() > NAME.length()) {
            throw new InvalidInputException("too many list train-sets arguments");
        }
    }
}
