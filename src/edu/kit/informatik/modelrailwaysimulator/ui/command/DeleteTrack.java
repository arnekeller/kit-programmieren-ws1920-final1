package edu.kit.informatik.modelrailwaysimulator.ui.command;

import edu.kit.informatik.modelrailwaysimulator.model.ModelRailwaySimulation;
import edu.kit.informatik.Terminal;
import edu.kit.informatik.modelrailwaysimulator.ui.CommandLine;
import edu.kit.informatik.modelrailwaysimulator.ui.InvalidInputException;

import static edu.kit.informatik.modelrailwaysimulator.ui.command.CommandFactory.NUMBER;

/**
 * Command used to delete a track or switch.
 *
 * @author Arne Keller
 * @version 1.0
 */
public class DeleteTrack extends Command {
    /**
     * Name of the delete track command.
     */
    public static final String NAME = "delete track";

    /**
     * Identifier of the rail to delete.
     */
    private int id;

    @Override
    public void apply(ModelRailwaySimulation simulation) {
        if (simulation.removeRail(id)) {
            Terminal.printLine(CommandLine.OK);
        } else {
            Terminal.printError("could not delete rail segment");
        }
    }

    @Override
    public void parse(String input) throws InvalidInputException {
        if (input == null || !input.startsWith(NAME)) {
            throw new InvalidInputException("unknown command");
        }
        final String argument = input.substring(NAME.length());
        if (!argument.matches(" " + NUMBER)) {
            throw new InvalidInputException("invalid/missing delete track argument");
        }
        id = Integer.parseInt(argument.substring(1));
    }
}
