package edu.kit.informatik.modelrailwaysimulator.ui.command;

import edu.kit.informatik.Terminal;
import edu.kit.informatik.modelrailwaysimulator.model.ModelRailwaySimulation;
import edu.kit.informatik.modelrailwaysimulator.ui.InvalidInputException;

import java.util.List;

/**
 * Command used to print a list of engines.
 *
 * @author Arne Keller
 * @version 1.0
 */
public class ListEngines extends Command {
    /**
     * Name of the list engines command.
     */
    public static final String NAME = "list engines";

    @Override
    public void apply(ModelRailwaySimulation simulation) {
        final List<String> engines = simulation.listEngines();
        if (engines.isEmpty()) {
            Terminal.printLine("No engine exists");
        } else {
            engines.forEach(Terminal::printLine);
        }
    }

    @Override
    public void parse(String input) throws InvalidInputException {
        if (input == null || !input.startsWith(NAME)) {
            throw new InvalidInputException("unknown command");
        }
        if (input.length() > NAME.length()) {
            throw new InvalidInputException("too many list engines arguments");
        }
    }
}
