package edu.kit.informatik.modelrailwaysimulator.ui.command;

import edu.kit.informatik.Terminal;
import edu.kit.informatik.modelrailwaysimulator.model.ModelRailwaySimulation;
import edu.kit.informatik.modelrailwaysimulator.ui.InvalidInputException;

import java.util.SortedMap;

/**
 * Command used to print a list of trains on the terminal.
 *
 * @author Arne Keller
 * @version 1.0
 */
public class ListTrains extends Command {
    /**
     * Name of the list train command.
     */
    public static final String NAME = "list trains";

    @Override
    public void apply(ModelRailwaySimulation simulation) {
        final SortedMap<Integer, String> trains = simulation.getTrains();
        if (trains.isEmpty()) {
            Terminal.printLine("No train exists");
        } else {
            trains.values().forEach(Terminal::printLine);
        }
    }

    @Override
    public void parse(String input) throws InvalidInputException {
        if (input == null || !input.startsWith(NAME)) {
            throw new InvalidInputException("unknown command");
        }
        if (input.length() > NAME.length()) {
            throw new InvalidInputException("too many list trains arguments");
        }
    }
}
