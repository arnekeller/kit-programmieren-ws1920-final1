package edu.kit.informatik.modelrailwaysimulator.ui.command;

import edu.kit.informatik.modelrailwaysimulator.model.Coach;
import edu.kit.informatik.modelrailwaysimulator.model.LogicException;
import edu.kit.informatik.modelrailwaysimulator.model.ModelRailwaySimulation;
import edu.kit.informatik.Terminal;
import edu.kit.informatik.modelrailwaysimulator.model.TrainSet;
import edu.kit.informatik.modelrailwaysimulator.ui.InvalidInputException;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static edu.kit.informatik.modelrailwaysimulator.ui.command.CommandFactory.ALPHANUMERIC_WORD;
import static edu.kit.informatik.modelrailwaysimulator.ui.command.CommandFactory.BOOL;
import static edu.kit.informatik.modelrailwaysimulator.ui.command.CommandFactory.NUMBER;

/**
 * Command used to add a new train set.
 *
 * @author Arne Keller
 * @version Arne Keller
 */
public class CreateTrainSet extends Command {
    /**
     * Name of the create train-set command.
     */
    public static final String NAME = "create train-set";
    private static final Pattern CREATE_TRAIN_SET_ARGUMENTS
            = Pattern.compile(" (" + ALPHANUMERIC_WORD + ") (" + ALPHANUMERIC_WORD + ") ("
            + NUMBER + ") (" + BOOL + ") (" + BOOL + ")");

    /**
     * Series (class) of the new train set.
     */
    private String series;
    /**
     * Name of the new train set.
     */
    private String name;
    /**
     * Length of the new train set.
     */
    private int length;
    /**
     * Whether the new train set should have front coupling.
     */
    private boolean couplingFront;
    /**
     * Whether the new train set should have a back coupling.
     */
    private boolean couplingBack;

    @Override
    public void apply(ModelRailwaySimulation simulation) throws LogicException {
        if (name == null) {
            throw new IllegalStateException("command not initialized");
        }
        final TrainSet trainSet = new TrainSet(series, name, length, couplingFront, couplingBack);
        simulation.createTrainSet(trainSet);
        Terminal.printLine(trainSet.getIdentifier());
    }

    @Override
    public void parse(String input) throws InvalidInputException {
        if (input == null || !input.startsWith(NAME)) {
            throw new InvalidInputException("unknown command");
        }
        final Matcher matcher = CREATE_TRAIN_SET_ARGUMENTS.matcher(input.substring(NAME.length()));
        if (!matcher.matches()) {
            throw new InvalidInputException("invalid create train-set arguments");
        }
        series = matcher.group(1);
        if (Coach.IDENTIFIER_PREFIX.equals(series)) {
            throw new InvalidInputException("invalid train-set class/series");
        }
        name = matcher.group(2);
        length = Integer.parseInt(matcher.group(3));
        couplingFront = Boolean.parseBoolean(matcher.group(4));
        couplingBack = Boolean.parseBoolean(matcher.group(5));
    }
}
