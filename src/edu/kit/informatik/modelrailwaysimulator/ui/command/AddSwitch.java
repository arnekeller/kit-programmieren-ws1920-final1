package edu.kit.informatik.modelrailwaysimulator.ui.command;

import edu.kit.informatik.modelrailwaysimulator.model.LogicException;
import edu.kit.informatik.modelrailwaysimulator.model.ModelRailwaySimulation;
import edu.kit.informatik.modelrailwaysimulator.model.Vector2D;
import edu.kit.informatik.Terminal;
import edu.kit.informatik.modelrailwaysimulator.ui.InvalidInputException;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static edu.kit.informatik.modelrailwaysimulator.ui.command.CommandFactory.VECTOR;

/**
 * Command used to add a switch to the rail network.
 *
 * @author Arne Keller
 * @version 1.0
 */
public class AddSwitch extends Command {
    /**
     * Name of the add switch command.
     */
    public static final String NAME = "add switch";
    private static final Pattern ADD_SWITCH_ARGUMENTS
            = Pattern.compile(" \\((" + VECTOR + ")\\) -> \\((" + VECTOR + ")\\),\\((" + VECTOR + ")\\)");

    /**
     * Start position of the switch to add.
     */
    private Vector2D start;
    /**
     * End position 1 of the switch to add.
     */
    private Vector2D end1;
    /**
     * End position 2 of the switch to add.
     */
    private Vector2D end2;

    @Override
    public void apply(ModelRailwaySimulation simulation) throws LogicException {
        if (start == null) {
            throw new IllegalStateException("command not initialized");
        }
        final int id = simulation.addSwitch(start, end1, end2);
        if (id == -1) {
            Terminal.printError("switch not connected to existing rails");
        } else {
            Terminal.printLine(id);
        }
    }

    @Override
    public void parse(String input) throws InvalidInputException {
        if (input == null || !input.startsWith(NAME)) {
            throw new InvalidInputException("unknown command");
        }
        final Matcher matcher = ADD_SWITCH_ARGUMENTS.matcher(input.substring(NAME.length()));
        if (!matcher.matches()) {
            throw new InvalidInputException("invalid add switch argument syntax");
        }
        start = Vector2D.parse(matcher.group(1));
        end1 = Vector2D.parse(matcher.group(2));
        end2 = Vector2D.parse(matcher.group(3));
    }
}
