package edu.kit.informatik.modelrailwaysimulator.ui.command;

import edu.kit.informatik.Terminal;
import edu.kit.informatik.modelrailwaysimulator.model.Coach;
import edu.kit.informatik.modelrailwaysimulator.model.Engine;
import edu.kit.informatik.modelrailwaysimulator.model.LogicException;
import edu.kit.informatik.modelrailwaysimulator.model.ModelRailwaySimulation;
import edu.kit.informatik.modelrailwaysimulator.ui.EngineType;
import edu.kit.informatik.modelrailwaysimulator.ui.InvalidInputException;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static edu.kit.informatik.modelrailwaysimulator.ui.EngineType.ENGINE_TYPE;
import static edu.kit.informatik.modelrailwaysimulator.ui.command.CommandFactory.ALPHANUMERIC_WORD;
import static edu.kit.informatik.modelrailwaysimulator.ui.command.CommandFactory.BOOL;
import static edu.kit.informatik.modelrailwaysimulator.ui.command.CommandFactory.NUMBER;

/**
 * Command used to create a single engine.
 *
 * @author Arne Keller
 * @version 1.0
 */
public class CreateEngine extends Command {
    /**
     * Name of the create engine command.
     */
    public static final String NAME = "create engine";
    private static final Pattern CREATE_ENGINE_ARGUMENTS
            = Pattern.compile(" (" + ENGINE_TYPE + ") (" + ALPHANUMERIC_WORD + ") (" + ALPHANUMERIC_WORD + ") ("
            + NUMBER + ") (" + BOOL + ") (" + BOOL + ")");

    /**
     * Type of the new engine.
     */
    private EngineType type;
    /**
     * Series (class) of the new engine.
     */
    private String series;
    /**
     * Name of the new engine.
     */
    private String name;
    /**
     * Length of the new engine.
     */
    private int length;
    /**
     * Whether the new engine should have a front coupling.
     */
    private boolean couplingFront;
    /**
     * Whether the new engine should have a back coupling.
     */
    private boolean couplingBack;

    @Override
    public void apply(ModelRailwaySimulation simulation) throws LogicException {
        if (type == null) {
            throw new IllegalStateException("command not initialized");
        }
        final Engine engine = type.createEngine(series, name, length, couplingFront, couplingBack);
        simulation.createEngine(engine);
        Terminal.printLine(engine.getIdentifier());
    }

    @Override
    public void parse(String input) throws InvalidInputException {
        if (input == null || !input.startsWith(NAME)) {
            throw new InvalidInputException("unknown command");
        }
        final Matcher matcher = CREATE_ENGINE_ARGUMENTS.matcher(input.substring(NAME.length()));
        if (!matcher.matches()) {
            throw new InvalidInputException("invalid create engine argument syntax");
        }
        type = EngineType.parse(matcher.group(1));
        series = matcher.group(2);
        if (Coach.IDENTIFIER_PREFIX.equals(series)) {
            throw new InvalidInputException("invalid engine class/series");
        }
        name = matcher.group(3);
        length = Integer.parseInt(matcher.group(4));
        couplingFront = Boolean.parseBoolean(matcher.group(5));
        couplingBack = Boolean.parseBoolean(matcher.group(6));
    }
}
