package edu.kit.informatik.modelrailwaysimulator.ui.command;

import edu.kit.informatik.Terminal;
import edu.kit.informatik.modelrailwaysimulator.model.LogicException;
import edu.kit.informatik.modelrailwaysimulator.model.ModelRailwaySimulation;
import edu.kit.informatik.modelrailwaysimulator.ui.InvalidInputException;

import static edu.kit.informatik.modelrailwaysimulator.ui.command.CommandFactory.NUMBER;

/**
 * Command used to print a train as ASCII art.
 *
 * @author Arne Keller
 * @version 1.0
 */
public class ShowTrain extends Command {
    /**
     * Name of the show train command.
     */
    public static final String NAME = "show train";

    /**
     * Identifier of the train to display.
     */
    private int id;

    @Override
    public void apply(ModelRailwaySimulation simulation) throws LogicException {
        simulation.showTrain(id).forEach(Terminal::printLine);
    }

    @Override
    public void parse(String input) throws InvalidInputException {
        if (input == null || !input.startsWith(NAME)) {
            throw new InvalidInputException("unknown command");
        }
        final String argument = input.substring(NAME.length());
        if (!argument.matches(" " + NUMBER)) {
            throw new InvalidInputException("invalid show train argument");
        }
        id = Integer.parseInt(argument.substring(1));
    }
}
