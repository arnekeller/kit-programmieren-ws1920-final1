package edu.kit.informatik.modelrailwaysimulator.ui.command;

import edu.kit.informatik.Terminal;
import edu.kit.informatik.modelrailwaysimulator.model.LogicException;
import edu.kit.informatik.modelrailwaysimulator.model.ModelRailwaySimulation;
import edu.kit.informatik.modelrailwaysimulator.model.Vector2D;
import edu.kit.informatik.modelrailwaysimulator.ui.CommandLine;
import edu.kit.informatik.modelrailwaysimulator.ui.InvalidInputException;

import java.util.List;
import java.util.Optional;
import java.util.SortedSet;

import static edu.kit.informatik.modelrailwaysimulator.ui.command.CommandFactory.NUMBER;

/**
 * Command used to advance the simulation.
 *
 * @author Arne Keller
 * @version 1.0
 */
public class Step extends Command {
    /**
     * Name of the step command.
     */
    public static final String NAME = "step";

    /**
     * Amount of steps to perform (= speed). Negative values cause the trains to move backward.
     */
    private short speed;

    @Override
    public void apply(ModelRailwaySimulation simulation) throws LogicException {
        final Optional<List<SortedSet<Integer>>> collisions = simulation.step(speed);
        if (!collisions.isPresent()) {
            // print OK if no there are trains placed
            Terminal.printLine(CommandLine.OK);
        } else {
            for (final int id : simulation.getTrains().keySet()) {
                // get collision with this train
                final SortedSet<Integer> collisionSet = collisions.get().stream()
                        // only get collision if this train has the lowest id in collision
                        // (avoids duplicates)
                        .filter(collision -> collision.first() == id)
                        .findFirst().orElse(null);
                if (collisionSet != null) { // print collision
                    Terminal.printLine("Crash of train " + String.join(",",
                            collisionSet.stream().map(Object::toString).toArray(String[]::new)));
                } else { // or position
                    final Vector2D position = simulation.getTrainPosition(id);
                    if (position != null) { // only print placed trains (skips crashed/derailed trains)
                        Terminal.printLine(String.format("Train %d at %s", id, position));
                    }
                }
            }
        }
    }

    @Override
    public void parse(String input) throws InvalidInputException {
        if (input == null || !input.startsWith(NAME)) {
            throw new InvalidInputException("unknown command");
        }
        final String argument = input.substring(NAME.length());
        if (!argument.matches(" " + NUMBER)) {
            throw new InvalidInputException("invalid step argument");
        }
        speed = Short.parseShort(argument.substring(1));
    }
}
