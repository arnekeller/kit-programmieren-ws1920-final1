package edu.kit.informatik.modelrailwaysimulator.ui.command;

import edu.kit.informatik.Terminal;
import edu.kit.informatik.modelrailwaysimulator.model.ModelRailwaySimulation;
import edu.kit.informatik.modelrailwaysimulator.ui.InvalidInputException;

import java.util.List;

/**
 * Command used to print a list of coaches.
 *
 * @author Arne Keller
 * @version 1.0
 */
public class ListCoaches extends Command {
    /**
     * Name of the list coaches command.
     */
    public static final String NAME = "list coaches";

    @Override
    public void apply(ModelRailwaySimulation simulation) {
        final List<String> coaches = simulation.listCoaches();
        if (coaches.isEmpty()) {
            Terminal.printLine("No coach exists");
        } else {
            coaches.forEach(Terminal::printLine);
        }
    }

    @Override
    public void parse(String input) throws InvalidInputException {
        if (input == null || !input.startsWith(NAME)) {
            throw new InvalidInputException("unknown command");
        }
        if (input.length() > NAME.length()) {
            throw new InvalidInputException("too many list coaches arguments");
        }
    }
}
