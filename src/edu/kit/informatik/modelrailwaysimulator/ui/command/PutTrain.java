package edu.kit.informatik.modelrailwaysimulator.ui.command;

import edu.kit.informatik.modelrailwaysimulator.model.LogicException;
import edu.kit.informatik.modelrailwaysimulator.model.ModelRailwaySimulation;
import edu.kit.informatik.modelrailwaysimulator.model.Vector2D;
import edu.kit.informatik.Terminal;
import edu.kit.informatik.modelrailwaysimulator.ui.CommandLine;
import edu.kit.informatik.modelrailwaysimulator.ui.InvalidInputException;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static edu.kit.informatik.modelrailwaysimulator.ui.command.CommandFactory.NUMBER;
import static edu.kit.informatik.modelrailwaysimulator.ui.command.CommandFactory.VECTOR;

/**
 * Command used to put a train on the rail network.
 *
 * @author Arne Keller
 * @version 1.0
 */
public class PutTrain extends Command {
    /**
     * Name of the put train command.
     */
    public static final String NAME = "put train";
    private static final Pattern PUT_TRAIN_ARGUMENTS
            = Pattern.compile(" (" + NUMBER + ") at \\((" + VECTOR + ")\\) in direction (" + VECTOR + ")");

    /**
     * Identifier of the train to place.
     */
    private int id;
    /**
     * Where to place the train.
     */
    private Vector2D point;
    /**
     * Initial direction of the train.
     */
    private Vector2D direction;

    @Override
    public void apply(ModelRailwaySimulation simulation) throws LogicException {
        if (point == null) {
            throw new IllegalStateException("command not initialized");
        }
        if (simulation.putTrain(id, point, direction)) {
            Terminal.printLine(CommandLine.OK);
        } else {
            Terminal.printError("could not place train");
        }
    }

    @Override
    public void parse(String input) throws InvalidInputException {
        if (input == null || !input.startsWith(NAME)) {
            throw new InvalidInputException("unknown command");
        }
        final Matcher matcher = PUT_TRAIN_ARGUMENTS.matcher(input.substring(NAME.length()));
        if (!matcher.matches()) {
            throw new InvalidInputException("invalid put train arguments");
        }
        id = Integer.parseInt(matcher.group(1));
        point = Vector2D.parse(matcher.group(2));
        direction = Vector2D.parse(matcher.group(3));
    }
}
