package edu.kit.informatik.modelrailwaysimulator.ui.command;

import edu.kit.informatik.modelrailwaysimulator.model.LogicException;
import edu.kit.informatik.modelrailwaysimulator.model.ModelRailwaySimulation;
import edu.kit.informatik.modelrailwaysimulator.model.RollingStock;
import edu.kit.informatik.Terminal;
import edu.kit.informatik.modelrailwaysimulator.ui.InvalidInputException;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static edu.kit.informatik.modelrailwaysimulator.ui.command.CommandFactory.NUMBER;
import static edu.kit.informatik.modelrailwaysimulator.ui.command.CommandFactory.ROLLING_STOCK_IDENTIFIER;

/**
 * Command used to construct a new train or modify an existing train.
 *
 * @author Arne Keller
 * @version 1.0
 */
public class AddTrain extends Command {
    /**
     * Name of the add train command.
     */
    public static final String NAME = "add train";
    private static final Pattern ADD_TRAIN_ARGUMENTS
            = Pattern.compile(" (" + NUMBER + ") (" + ROLLING_STOCK_IDENTIFIER + ")");

    /**
     * Identifier of the train to add or modify.
     */
    private int trainId;
    /**
     * Identifier of the rolling stock to add.
     */
    private String rollingStockId;

    @Override
    public void apply(ModelRailwaySimulation simulation) throws LogicException {
        if (rollingStockId == null) {
            throw new IllegalStateException("command not initialized");
        }
        final RollingStock rollingStock = simulation.addTrain(trainId, rollingStockId);
        Terminal.printLine(String.format("%s %s added to train %d",
                rollingStock.description(), rollingStock.getIdentifier(), trainId));
    }

    @Override
    public void parse(String input) throws InvalidInputException {
        if (input == null || !input.startsWith(NAME)) {
            throw new InvalidInputException("unknown command");
        }
        final Matcher matcher = ADD_TRAIN_ARGUMENTS.matcher(input.substring(NAME.length()));
        if (!matcher.matches()) {
            throw new InvalidInputException("invalid add train arguments");
        }
        trainId = Integer.parseInt(matcher.group(1));
        rollingStockId = matcher.group(2);
    }
}
