package edu.kit.informatik.modelrailwaysimulator.ui.command;

import edu.kit.informatik.modelrailwaysimulator.model.LogicException;
import edu.kit.informatik.modelrailwaysimulator.model.ModelRailwaySimulation;
import edu.kit.informatik.modelrailwaysimulator.model.Vector2D;
import edu.kit.informatik.Terminal;
import edu.kit.informatik.modelrailwaysimulator.ui.InvalidInputException;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static edu.kit.informatik.modelrailwaysimulator.ui.command.CommandFactory.VECTOR;

/**
 * Command used to add a track to the rail network.
 *
 * @author Arne Keller
 * @version 1.0
 */
public class AddTrack extends Command {
    /**
     * Name of the add track command.
     */
    public static final String NAME = "add track";
    private static final Pattern ADD_TRACK_ARGUMENTS
            = Pattern.compile(" \\((" + VECTOR + ")\\) -> \\((" + VECTOR + ")\\)");

    /**
     * Start position of the new track.
     */
    private Vector2D start;
    /**
     * End position of the new track.
     */
    private Vector2D end;

    @Override
    public void apply(ModelRailwaySimulation simulation) throws LogicException {
        if (start == null) {
            throw new IllegalStateException("command not initialized");
        }
        final int id = simulation.addTrack(start, end);
        if (id == -1) {
            Terminal.printError("id space exhausted");
        } else {
            Terminal.printLine(id);
        }
    }

    @Override
    public void parse(String input) throws InvalidInputException {
        if (input == null || !input.startsWith(NAME)) {
            throw new InvalidInputException("unknown command");
        }
        final Matcher matcher = ADD_TRACK_ARGUMENTS.matcher(input.substring(NAME.length()));
        if (!matcher.matches()) {
            throw new InvalidInputException("invalid add track argument syntax");
        }
        start = Vector2D.parse(matcher.group(1));
        end = Vector2D.parse(matcher.group(2));
    }
}
