package edu.kit.informatik.modelrailwaysimulator.ui.command;

import edu.kit.informatik.modelrailwaysimulator.model.Coach;
import edu.kit.informatik.modelrailwaysimulator.ui.InvalidInputException;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.function.Supplier;

/**
 * Utility class used to parse user input into commands.
 *
 * @author Arne Keller
 * @version 1.0
 */
public final class CommandFactory {
    /**
     * Regex to accept one boolean (true or false).
     */
    public static final String BOOL = "true|false";
    /**
     * Regex to match a number, accepting leading plus sign and zeros.
     */
    public static final String NUMBER = "[+-]?\\d+";
    /**
     * Regex to accept two numbers separated by a comma.
     */
    public static final String VECTOR = NUMBER + "," + NUMBER;
    /**
     * Regex to accept one word consisting of letters and digits.
     */
    public static final String ALPHANUMERIC_WORD = "[\\p{L}\\d]+";
    /**
     * Regex to accept one rolling stock identifier.
     */
    public static final String ROLLING_STOCK_IDENTIFIER
            = "(" + ALPHANUMERIC_WORD + "-" + ALPHANUMERIC_WORD + ")|" + Coach.IDENTIFIER_PATTERN;

    /**
     * Map of command names (prefixes) to Command constructors.
     */
    private static final Map<String, Supplier<Command>> COMMANDS = new HashMap<>();

    static {
        // initialize registered commands
        COMMANDS.put(AddTrack.NAME, AddTrack::new);
        COMMANDS.put(AddSwitch.NAME, AddSwitch::new);
        COMMANDS.put(DeleteTrack.NAME, DeleteTrack::new);
        COMMANDS.put(ListTracks.NAME, ListTracks::new);
        COMMANDS.put(SetSwitch.NAME, SetSwitch::new);
        COMMANDS.put(CreateEngine.NAME, CreateEngine::new);
        COMMANDS.put(ListEngines.NAME, ListEngines::new);
        COMMANDS.put(CreateCoach.NAME, CreateCoach::new);
        COMMANDS.put(ListCoaches.NAME, ListCoaches::new);
        COMMANDS.put(CreateTrainSet.NAME, CreateTrainSet::new);
        COMMANDS.put(ListTrainSets.NAME, ListTrainSets::new);
        COMMANDS.put(DeleteRollingStock.NAME, DeleteRollingStock::new);
        COMMANDS.put(AddTrain.NAME, AddTrain::new);
        COMMANDS.put(DeleteTrain.NAME, DeleteTrain::new);
        COMMANDS.put(ListTrains.NAME, ListTrains::new);
        COMMANDS.put(ShowTrain.NAME, ShowTrain::new);
        COMMANDS.put(PutTrain.NAME, PutTrain::new);
        COMMANDS.put(Step.NAME, Step::new);
    }

    /**
     * Utility class: private constructor.
     */
    private CommandFactory() {

    }

    /**
     * Parse a single line of user input into one command.
     *
     * @param input user input line
     * @return a fully specified command object
     * @throws InvalidInputException if user input is invalid
     */
    public static Command getCommand(String input) throws InvalidInputException {
        final Optional<Command> matchingCommand = COMMANDS.keySet().stream()
                .filter(input::startsWith)
                .map(COMMANDS::get).map(Supplier::get)
                .findAny();
        if (!matchingCommand.isPresent()) {
            throw new InvalidInputException("unknown command");
        }
        final Command command = matchingCommand.get();
        command.parse(input);
        return command;
    }
}
