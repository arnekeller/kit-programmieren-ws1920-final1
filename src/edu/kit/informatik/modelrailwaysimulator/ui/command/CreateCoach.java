package edu.kit.informatik.modelrailwaysimulator.ui.command;

import edu.kit.informatik.modelrailwaysimulator.model.LogicException;
import edu.kit.informatik.modelrailwaysimulator.ui.CoachType;
import edu.kit.informatik.modelrailwaysimulator.model.ModelRailwaySimulation;
import edu.kit.informatik.Terminal;
import edu.kit.informatik.modelrailwaysimulator.ui.InvalidInputException;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static edu.kit.informatik.modelrailwaysimulator.ui.CoachType.COACH_TYPE;
import static edu.kit.informatik.modelrailwaysimulator.ui.command.CommandFactory.BOOL;
import static edu.kit.informatik.modelrailwaysimulator.ui.command.CommandFactory.NUMBER;

/**
 * Command used to create a single coach.
 *
 * @author Arne Keller
 * @version 1.0
 */
public class CreateCoach extends Command {
    /**
     * Name of the create coach command.
     */
    public static final String NAME = "create coach";
    private static final Pattern CREATE_COACH_ARGUMENTS
            = Pattern.compile(" (" + COACH_TYPE + ") (" + NUMBER + ") (" + BOOL + ") (" + BOOL + ")");

    /**
     * Type of the new coach.
     */
    private CoachType type;
    /**
     * Length of the new coach.
     */
    private int length;
    /**
     * Whether the new coach should have a front coupling.
     */
    private boolean couplingFront;
    /**
     * Whether the new coach should have a back coupling.
     */
    private boolean couplingBack;

    @Override
    public void apply(ModelRailwaySimulation simulation) throws LogicException {
        if (type == null) {
            throw new IllegalStateException("command not initialized");
        }
        final int id = simulation.createCoach(type, length, couplingFront, couplingBack);
        if (id == -1) {
            Terminal.printError("id space exhausted");
        } else {
            Terminal.printLine(id);
        }
    }

    @Override
    public void parse(String input) throws InvalidInputException {
        if (input == null || !input.startsWith(NAME)) {
            throw new InvalidInputException("unknown command");
        }
        final Matcher matcher = CREATE_COACH_ARGUMENTS.matcher(input.substring(NAME.length()));
        if (!matcher.matches()) {
            throw new InvalidInputException("invalid create coach arguments");
        }
        type = CoachType.parse(matcher.group(1));
        length = Integer.parseInt(matcher.group(2));
        couplingFront = Boolean.parseBoolean(matcher.group(3));
        couplingBack = Boolean.parseBoolean(matcher.group(4));
    }
}
