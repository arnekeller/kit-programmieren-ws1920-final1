package edu.kit.informatik.modelrailwaysimulator.ui.command;

import edu.kit.informatik.modelrailwaysimulator.model.ModelRailwaySimulation;
import edu.kit.informatik.Terminal;
import edu.kit.informatik.modelrailwaysimulator.ui.CommandLine;
import edu.kit.informatik.modelrailwaysimulator.ui.InvalidInputException;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static edu.kit.informatik.modelrailwaysimulator.ui.command.CommandFactory.ROLLING_STOCK_IDENTIFIER;

/**
 * Command used to delete rolling stock.
 *
 * @author Arne Keller
 * @version 1.0
 */
public class DeleteRollingStock extends Command {
    /**
     * Name of the delete rolling stock command.
     */
    public static final String NAME = "delete rolling stock";
    private static final Pattern DELETE_ROLLING_STOCK_ARGUMENT = Pattern.compile(" (" + ROLLING_STOCK_IDENTIFIER + ")");

    /**
     * Identifier of the rolling stock to delete.
     */
    private String id;

    @Override
    public void apply(ModelRailwaySimulation simulation) {
        if (id == null) {
            throw new IllegalStateException("command not initialized");
        }
        if (simulation.deleteRollingStock(id)) {
            Terminal.printLine(CommandLine.OK);
        } else {
            Terminal.printError("could not delete rolling stock");
        }
    }

    @Override
    public void parse(String input) throws InvalidInputException {
        if (input == null || !input.startsWith(NAME)) {
            throw new InvalidInputException("unknown command");
        }
        final Matcher matcher = DELETE_ROLLING_STOCK_ARGUMENT.matcher(input.substring(NAME.length()));
        if (!matcher.matches()) {
            throw new InvalidInputException("invalid delete rolling stock argument");
        }
        id = matcher.group(1);
    }
}
