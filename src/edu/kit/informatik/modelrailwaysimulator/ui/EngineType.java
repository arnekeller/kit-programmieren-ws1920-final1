package edu.kit.informatik.modelrailwaysimulator.ui;

import edu.kit.informatik.modelrailwaysimulator.model.DieselEngine;
import edu.kit.informatik.modelrailwaysimulator.model.ElectricalEngine;
import edu.kit.informatik.modelrailwaysimulator.model.Engine;
import edu.kit.informatik.modelrailwaysimulator.model.LogicException;
import edu.kit.informatik.modelrailwaysimulator.model.SteamEngine;

/**
 * Type of locomotive. Can be either diesel, steam or electrical.
 *
 * @author Arne Keller
 * @version 1.0
 */
public enum EngineType {
    /**
     * Diesel engine.
     */
    DIESEL,
    /**
     * Steam engine.
     */
    STEAM,
    /**
     * Electrical engine.
     */
    ELECTRICAL;

    /**
     * Regex to match one engine type.
     */
    public static final String ENGINE_TYPE = "diesel|steam|electrical";

    /**
     * Create a new engine of this type and the specified parameters.

     * @param series engine series/class
     * @param name engine name
     * @param length engine length
     * @param couplingFront whether the engine should have a front coupling
     * @param couplingBack whether the engine should have a back coupling
     * @return new engine
     * @throws LogicException on invalid input (e.g. zero-sized engine)
     */
    public Engine createEngine(String series, String name, int length, boolean couplingFront, boolean couplingBack)
            throws LogicException {
        switch (this) {
            case ELECTRICAL:
                return new ElectricalEngine(series, name, length, couplingFront, couplingBack);
            case STEAM:
                return new SteamEngine(series, name, length, couplingFront, couplingBack);
            case DIESEL:
                return new DieselEngine(series, name, length, couplingFront, couplingBack);
            default:
                throw new IllegalStateException("this is null");
        }
    }

    /**
     * Parse the textual representation of a engine type into the correct enum value.
     *
     * @param value engine type as text
     * @return engine type as enum, or null if invalid
     */
    public static EngineType parse(String value) {
        switch (value) {
            case "diesel":
                return DIESEL;
            case "steam":
                return STEAM;
            case "electrical":
                return ELECTRICAL;
            default:
                return null;
        }
    }
}
