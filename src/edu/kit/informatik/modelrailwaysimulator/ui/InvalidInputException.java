package edu.kit.informatik.modelrailwaysimulator.ui;

/**
 * Thrown on invalid user input.
 *
 * @author Arne Keller
 * @version 1.0
 */
public class InvalidInputException extends Exception {
    /**
     * Construct a new invalid input exception with a specific message.
     *
     * @param message the error message
     */
    public InvalidInputException(String message) {
        super(message);
    }
}
