package edu.kit.informatik.modelrailwaysimulator.ui;

import edu.kit.informatik.modelrailwaysimulator.model.Coach;
import edu.kit.informatik.modelrailwaysimulator.model.FreightCoach;
import edu.kit.informatik.modelrailwaysimulator.model.LogicException;
import edu.kit.informatik.modelrailwaysimulator.model.PassengerCoach;
import edu.kit.informatik.modelrailwaysimulator.model.SpecialCoach;

/**
 * Type of a coach.
 *
 * @author Arne Keller
 * @version 1.0
 */
public enum CoachType {
    /**
     * Passenger coach.
     */
    PASSENGER,
    /**
     * Freight coach.
     */
    FREIGHT,
    /**
     * Special coach used for e.g. firefighting.
     */
    SPECIAL;

    /**
     * Regex to match one coach type.
     */
    public static final String COACH_TYPE = "passenger|freight|special";

    /**
     * Create a coach of this type with the specified parameters.
     *
     * @param id coach identifier
     * @param length coach length
     * @param couplingFront whether the coach should have a front coupling
     * @param couplingBack whether the coach should have a back coupling
     * @return new coach
     * @throws LogicException on invalid input (e.g. zero-sized coach)
     */
    public Coach createCoach(int id, int length, boolean couplingFront, boolean couplingBack)
            throws LogicException {
        switch (this) {
            case PASSENGER:
                return new PassengerCoach(id, length, couplingFront, couplingBack);
            case FREIGHT:
                return new FreightCoach(id, length, couplingFront, couplingBack);
            case SPECIAL:
                return new SpecialCoach(id, length, couplingFront, couplingBack);
            default:
                throw new IllegalArgumentException("this is null");
        }
    }

    /**
     * Parse the textual representation of a coach type into the correct enum value.
     * @param value coach type as text
     * @return coach type as enum, or null if invalid
     */
    public static CoachType parse(String value) {
        switch (value) {
            case "passenger":
                return PASSENGER;
            case "freight":
                return FREIGHT;
            case "special":
                return SPECIAL;
            default:
                return null;
        }
    }
}
