package edu.kit.informatik.modelrailwaysimulator.ui;

import edu.kit.informatik.modelrailwaysimulator.model.LogicException;
import edu.kit.informatik.modelrailwaysimulator.model.ModelRailwaySimulation;
import edu.kit.informatik.Terminal;
import edu.kit.informatik.modelrailwaysimulator.ui.command.Command;
import edu.kit.informatik.modelrailwaysimulator.ui.command.CommandFactory;

/**
 * Interactive simulation runner: gets user inputs and processes specified commands.
 *
 * @author Arne Keller
 * @version 1.0
 */
public final class CommandLine {
    /**
     * Text used to indicate that an operation was successful.
     */
    public static final String OK = "OK";

    /**
     * Command used to exit the simulation and terminate the program.
     */
    private static final String EXIT = "exit";

    /**
     * Utility class: private constructor.
     */
    private CommandLine() {

    }

    /**
     * Start a new interactive user session. Returns when standard input is fully processed or user exits.
     */
    public static void startInteractive() {
        final ModelRailwaySimulation simulation = new ModelRailwaySimulation();

        while (true) {
            final String input = Terminal.readLine();

            if (input.startsWith(EXIT)) {
                if (input.length() != EXIT.length()) {
                    Terminal.printError("input after exit command");
                    continue;
                } else {
                    return;
                }
            }

            final Command command;
            try {
                command = CommandFactory.getCommand(input);
            } catch (final NumberFormatException | InvalidInputException e) {
                Terminal.printError("input error: " + e.getMessage());
                continue;
            }
            try {
                command.apply(simulation);
            } catch (final NumberFormatException | LogicException e) {
                Terminal.printError("logic error: " + e.getMessage());
            }
        }
    }
}
