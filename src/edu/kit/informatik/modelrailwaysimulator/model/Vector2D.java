package edu.kit.informatik.modelrailwaysimulator.model;

import java.util.Objects;

/**
 * A two-dimensional vector, usually a position or movement direction vector.
 * An object of this class is immutable.
 *
 * @author Arne Keller
 * @version 1.1
 */
public class Vector2D {
    /**
     * First coordinate of the vector, usually referred to as 'x'.
     */
    private final long x;
    /**
     * Second coordinate of the vector, usually referred to as 'y'.
     */
    private final long y;

    /**
     * Construct a new vector.
     *
     * @param x first component
     * @param y second component
     */
    public Vector2D(long x, long y) {
        this.x = x;
        this.y = y;
    }

    /**
     * Parse two numbers separated by a comma. Note that this method only accepts (signed) 32-bit integers!
     *
     * @param input two 32-bit numbers separated by a comma
     * @return a vector containing the two numbers, null otherwise
     */
    public static Vector2D parse(String input) {
        final String[] coordinates = input.split(",", 2);
        final int x = Integer.parseInt(coordinates[0]);
        final int y = Integer.parseInt(coordinates[1]);
        return new Vector2D(x, y);
    }

    /**
     * Format the vector like this: (x,y).
     *
     * @return textual representation of this vector
     */
    @Override
    public String toString() {
        return String.format("(%d,%d)", x, y);
    }

    /**
     * Calculate the distance between this vector (interpreted as position) and another position.
     *
     * @param other the point to measure distance to
     * @return the manhattan distance
     */
    public long distanceTo(Vector2D other) {
        return Math.abs(this.x - other.x) + Math.abs(this.y - other.y);
    }

    /**
     * Check whether this vector represents a valid direction. Is equivalent to checking whether it is parallel to
     * the x-axis or the y-axis.
     *
     * @return whether this vector is a valid direction
     */
    public boolean isDirection() {
        return (x == 0 || y == 0) && (x != 0 || y != 0);
    }

    /**
     * Check whether this vector is parallel to another vector. Note that this method only works for vectors that
     * {@link #isDirection are directions}.
     *
     * @param other vector to compare
     * @return whether both are parallel
     */
    public boolean isParallelTo(Vector2D other) {
        return x == 0 && other.x == 0 || y == 0 && other.y == 0;
    }

    /**
     * Calculate whether this vector points in the same direction as another. Note that this method only works for
     * vectors that {@link #isDirection are directions}.
     *
     * @param other vector to compare
     * @return whether they represent the same direction
     */
    public boolean directionEquals(Vector2D other) {
        return x == 0 && x == other.x && Long.signum(y) == Long.signum(other.y)
                || y == 0 && y == other.y && Long.signum(x) == Long.signum(other.x);
    }

    /**
     * Add two vectors and return their sum.
     *
     * @param movement vector to add
     * @return component-wise sum of this vector and the other vector
     */
    public Vector2D add(Vector2D movement) {
        return new Vector2D(x + movement.x, y + movement.y);
    }

    /**
     * Subtract another vector from this one and return the difference.
     *
     * @param other another vector
     * @return the difference of this vector and the other vector
     */
    public Vector2D subtract(Vector2D other) {
        return new Vector2D(x - other.x, y - other.y);
    }

    /**
     * Negate this vector component-wise.
     *
     * @return (-1) * this vector
     */
    public Vector2D negated() {
        return new Vector2D(-x, -y);
    }

    /**
     * Calculate multiplier * this, and return a new vector.
     *
     * @param multiplier any scalar
     * @return scaled vector
     */
    public Vector2D scale(long multiplier) {
        return new Vector2D(x * multiplier, y * multiplier);
    }

    /**
     * Normalize this vector. Will essentially return a new vector with (x/|x|, y/|y|).
     *
     * @return a vector with separately (!) normalized components
     */
    public Vector2D normalized() {
        if (x >= 0 && x <= 1 && y >= 0 && y <= 1) {
            return this;
        } else if (x != 0 && y != 0) {
            return new Vector2D(x / Math.abs(x), y / Math.abs(y));
        } else if (x != 0) {
            return new Vector2D(x / Math.abs(x), 0);
        } else { // y != 0
            return new Vector2D(0, y / Math.abs(y));
        }
    }

    /**
     * @return the first component of this vector
     */
    public long getX() {
        return x;
    }

    /**
     * @return the second component of this vector
     */
    public long getY() {
        return y;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj != null && getClass().equals(obj.getClass())) {
            final Vector2D point = (Vector2D) obj;

            return x == point.x && y == point.y;
        }

        return false;
    }

    @Override
    public int hashCode() {
        return Objects.hash(x, y);
    }
}
