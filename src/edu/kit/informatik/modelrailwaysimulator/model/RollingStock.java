package edu.kit.informatik.modelrailwaysimulator.model;

/**
 * A rolling stock with a specific integer length and couplings. Is usually an engine, train set or coach.
 *
 * @author Arne Keller
 * @version 1.0
 */
public abstract class RollingStock {
    /**
     * Length of this rolling stock.
     */
    private final int length;
    /**
     * Whether this rolling stock has a front coupling.
     */
    private final boolean couplingFront;
    /**
     * Whether this rolling stack has a back coupling.
     */
    private final boolean couplingBack;

    /**
     * Initialize a rolling stock.
     *
     * @param length length of this rolling stock
     * @param couplingFront whether this rolling stock should have a front coupling
     * @param couplingBack whether this rolling stock should have a back coupling
     * @throws LogicException on invalid user input (e.g. zero-sized coach)
     */
    protected RollingStock(int length, boolean couplingFront, boolean couplingBack) throws LogicException {
        if (length < 1) {
            throw new LogicException("rolling stock length has to be positive");
        }
        if (!couplingFront && !couplingBack) {
            throw new LogicException("rolling stocks needs at least one coupling");
        }
        this.length = length;
        this.couplingFront = couplingFront;
        this.couplingBack = couplingBack;
    }

    /**
     * @return identifier of this rolling stock
     */
    public abstract String getIdentifier();

    /**
     * @return length of this rolling stock
     */
    public int getLength() {
        return length;
    }

    /**
     * @return whether this rolling stock has a front coupling
     */
    public boolean hasCouplingFront() {
        return couplingFront;
    }

    /**
     * @return whether this rolling stock has a back coupling
     */
    public boolean hasCouplingBack() {
        return couplingBack;
    }

    /**
     * Check whether this rolling stock can theoretically couple to another rolling stock. This method will not check
     * whether the other rolling stock has special coupling requirements, but it will consider the special requirements
     * of this rolling stock.
     *
     * @param rollingStock other rolling stock
     * @return whether this rolling stock can couple to the other rolling stock
     */
    public abstract boolean canCoupleTo(RollingStock rollingStock);

    /**
     * Check whether this rolling stock is useful at the end of a train. Useful is defined as making the train move.
     * Especially useful for checking whether a train is valid before placing it.
     *
     * @return whether this rolling stock is useful at the end of a train
     */
    public abstract boolean usefulAtEndOfTrain();

    /**
     * @return ASCII art representation of this rolling stock
     */
    public abstract String[] textRepresentation();

    /**
     * Get a description of this rolling stock. Usually a few words (e.g. "passenger coach").
     *
     * @return short textual description of this rolling stock
     */
    public abstract String description();
}
