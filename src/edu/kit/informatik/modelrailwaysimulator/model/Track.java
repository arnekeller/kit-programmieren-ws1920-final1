package edu.kit.informatik.modelrailwaysimulator.model;

import java.util.Objects;

/**
 * A straight track. An object of this class is immutable.
 *
 * @author Arne Keller
 * @version 1.0
 */
public final class Track extends Rail {
    /**
     * Where this track starts.
     */
    private final Vector2D start;
    /**
     * Where this track ends.
     */
    private final Vector2D end;

    /**
     * Construct a new track. Start and end positions have to be on a straight line!
     *
     * @param start start position of the track
     * @param end end position of the track
     * @param id identifier to use
     * @throws LogicException if the positions are not on a straight line
     */
    public Track(Vector2D start, Vector2D end, int id) throws LogicException {
        super(id);
        if (start.distanceTo(end) == 0) {
            throw new LogicException("track has length zero");
        } else if (start.getX() != end.getX() && start.getY() != end.getY()) {
            throw new LogicException("invalid track segment: not a straight line");
        }
        this.start = start;
        this.end = end;
    }

    @Override
    public boolean canConnectTo(Vector2D point) {
        return point.equals(start) || point.equals(end);
    }

    @Override
    public boolean connectsTo(Vector2D point) {
        return canConnectTo(point);
    }

    @Override
    public boolean canConnectToRail(Rail rail) {
        return rail.canConnectTo(start) || rail.canConnectTo(end);
    }

    @Override
    public boolean isReadyForTrains() {
        return true;
    }

    @Override
    public boolean contains(Vector2D position) {
        if (position == null) {
            return false;
        }
        if (start.getX() == end.getX() && position.getX() == start.getX()) {
            return start.getY() < position.getY() && position.getY() < end.getY()
                    || start.getY() > position.getY() && position.getY() > end.getY();
        } else if (start.getY() == end.getY() && position.getY() == start.getY()) {
            return start.getX() < position.getX() && position.getX() < end.getX()
                    || start.getX() > position.getX() && position.getX() > end.getX();
        }
        return false;
    }

    @Override
    public Vector2D move(Vector2D position, Vector2D direction, long steps) {
        if (!connectsTo(position) && !contains(position)) {
            return null;
        }
        // attempt to move exactly in the specified direction
        final Vector2D nextPosition = position.add(direction.scale(steps));
        if (contains(nextPosition) || connectsTo(nextPosition)) {
            return nextPosition;
        } else if (direction.equals(getDirectionFrom(start))) {
            return end; // can not go further than end of rail
        } else if (direction.equals(getDirectionFrom(end))) {
            return start;
        } else if (position.equals(end) && !direction.equals(getDirectionFrom(start))) {
            return move(position, getDirectionFrom(end), steps); // attempt to correct direction
        } else if (position.equals(start) && !direction.equals(getDirectionFrom(end))) {
            return move(position, getDirectionFrom(start), steps);
        }
        return null;
    }

    @Override
    public Vector2D getDirectionFrom(Vector2D position) {
        if (start.equals(position)) {
            return new Vector2D(Long.signum(end.getX() - start.getX()), Long.signum(end.getY() - start.getY()));
        } else if (end.equals(position)) {
            return new Vector2D(Long.signum(start.getX() - end.getX()), Long.signum(start.getY() - end.getY()));
        } else {
            // in the middle of track, simply return direction from start
            return getDirectionFrom(start);
        }
    }

    @Override
    public boolean allowsPlacement(Vector2D position, Vector2D direction) {
        return contains(position) && getDirectionFrom(position).isParallelTo(direction)
                || connectsTo(position) && getDirectionFrom(position).negated().equals(direction);
    }

    @Override
    public boolean allowsMovement(Vector2D position, Vector2D direction) {
        return contains(position) && getDirectionFrom(position).isParallelTo(direction)
                || connectsTo(position) && getDirectionFrom(position).equals(direction);
    }

    /**
     * Get the start position of this track.
     *
     * @return start position of this track
     */
    public Vector2D getStart() {
        return start;
    }

    /**
     * Get the end position of this track.
     *
     * @return end position of this track
     */
    public Vector2D getEnd() {
        return end;
    }

    @Override
    public long getLength() {
        return start.distanceTo(end);
    }

    /**
     * Get the textual representation of this track in the format "t [id] [start] -> [end] [length]".
     *
     * @return textual representation of this track
     */
    @Override
    public String toString() {
        return String.format("t %d %s -> %s %d", getIdentifier(), start, end, getLength());
    }

    /**
     * Check whether two tracks are equal, ignoring their identifiers.
     *
     * @return whether this track is equal to another
     */
    @Override
    public boolean equals(Object obj) {
        if (obj != null && getClass().equals(obj.getClass())) {
            final Track track = (Track) obj;

            return start.equals(track.start) && end.equals(track.end);
        }

        return false;
    }

    @Override
    public int hashCode() {
        return Objects.hash(start, end);
    }
}
