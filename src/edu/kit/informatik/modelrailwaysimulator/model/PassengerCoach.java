package edu.kit.informatik.modelrailwaysimulator.model;

/**
 * A passenger coach.
 *
 * @author Arne Keller
 * @version 1.0
 */
public class PassengerCoach extends Coach {
    /**
     * ASCII art representation of a passenger coach.
     */
    private static final String[] PASSENGER_TEXT = new String[]{
        "____________________",
        "|  ___ ___ ___ ___ |",
        "|  |_| |_| |_| |_| |",
        "|__________________|",
        "|__________________|",
        "   (O)        (O)   "
    };

    /**
     * Construct a new passenger coach.
     *
     * @param identifier identifier to use
     * @param length length of coach
     * @param couplingFront whether the coach should have a front coupling
     * @param couplingBack whether the coach should have a back coupling
     * @throws LogicException on invalid user input (e.g. zero-sized coach)
     */
    public PassengerCoach(int identifier, int length, boolean couplingFront, boolean couplingBack)
            throws LogicException {
        super(identifier, length, couplingFront, couplingBack);
    }

    @Override
    public String description() {
        return "passenger coach";
    }

    @Override
    public String[] textRepresentation() {
        // make sure caller can not modify private data
        return PASSENGER_TEXT.clone();
    }

    @Override
    public String getType() {
        return "p";
    }
}
