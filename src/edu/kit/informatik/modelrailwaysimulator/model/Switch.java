package edu.kit.informatik.modelrailwaysimulator.model;

import java.util.Objects;

/**
 * A switch in the rail network. It connects a start position to one of two other positions.
 *
 * @author Arne Keller
 * @version 1.0
 */
public final class Switch extends Rail {
    /**
     * First leg of the switch.
     */
    private final Track positionOne;
    /**
     * Second leg of the switch.
     */
    private final Track positionTwo;
    /**
     * Currently selected position (either {@link #positionOne} or {@link #positionTwo}).
     */
    private Track selection;

    /**
     * Construct a new switch.
     *
     * @param start start position
     * @param end1 first end position
     * @param end2 second end position
     * @param id identifier of this switch
     * @throws LogicException if the switch is not composed of straight lines, or has zero-length parts
     */
    public Switch(Vector2D start, Vector2D end1, Vector2D end2, int id) throws LogicException {
        super(id);
        if (start.distanceTo(end1) == 0 || start.distanceTo(end2) == 0 || end1.distanceTo(end2) == 0) {
            throw new LogicException("switch has length zero");
        }
        this.positionOne = new Track(start, end1, 1);
        this.positionTwo = new Track(start, end2, 1);
    }

    @Override
    public boolean canConnectTo(Vector2D point) {
        return positionOne.canConnectTo(point) || positionTwo.canConnectTo(point);
    }

    /**
     * @param point point to check for connection
     * @return whether the currently active configuration connects to the position
     */
    @Override
    public boolean connectsTo(Vector2D point) {
        return selection != null && selection.canConnectTo(point);
    }

    /**
     * @param rail rail to check for possible connection
     * @return whether any of the configurations can connect to the position
     */
    @Override
    public boolean canConnectToRail(Rail rail) {
        return rail.canConnectToRail(positionOne) || rail.canConnectToRail(positionTwo);
    }

    /**
     * Reconfigure this switch. Allows setting the same position again.
     *
     * @param position point to connect to
     * @return whether switching was successful
     */
    @Override
    public boolean switchTo(Vector2D position) {
        if (positionOne.canConnectTo(position)) {
            selection = positionOne;
            return true;
        } else if (positionTwo.canConnectTo(position)) {
            selection = positionTwo;
            return true;
        }
        return false;
    }

    /**
     * @return whether this switch is set
     */
    @Override
    public boolean isReadyForTrains() {
        return selection != null;
    }

    /**
     * @param position the point to check
     * @return whether the active switch configuration {@link Track#contains(Vector2D) contains} that position
     */
    @Override
    public boolean contains(Vector2D position) {
        return selection != null && selection.contains(position);
    }

    @Override
    public Vector2D move(Vector2D position, Vector2D direction, long steps) {
        if (contains(position) || connectsTo(position)) {
            return selection.move(position, direction, steps);
        } else {
            return null;
        }
    }

    @Override
    public Vector2D getDirectionFrom(Vector2D position) {
        return selection.getDirectionFrom(position);
    }

    @Override
    public boolean allowsPlacement(Vector2D position, Vector2D direction) {
        return selection.allowsPlacement(position, direction);
    }

    @Override
    public boolean allowsMovement(Vector2D position, Vector2D direction) {
        return positionOne.allowsMovement(position, direction)
                || positionTwo.allowsMovement(position, direction);
    }

    /**
     * @return length of the current configuration
     */
    @Override
    public long getLength() {
        return selection.getLength();
    }

    /**
     * Get the textual representation of this switch in the format "t [id] [start] -> [end1],[end2] [length]".
     * Only returns length if set up.
     *
     * @return textual representation of this switch
     */
    @Override
    public String toString() {
        if (selection == null) {
            return String.format("s %d %s -> %s,%s", getIdentifier(),
                    positionOne.getStart(), positionOne.getEnd(), positionTwo.getEnd());
        } else { // print length of active segment
            return String.format("s %d %s -> %s,%s %d", getIdentifier(),
                    positionOne.getStart(), positionOne.getEnd(), positionTwo.getEnd(), getLength());
        }
    }

    /**
     * Check whether two switches are equal, ignoring the current configuration and their identifiers.
     *
     * @return whether this switch is equal to the other
     */
    @Override
    public boolean equals(Object obj) {
        if (obj != null && getClass().equals(obj.getClass())) {
            final Switch otherSwitch = (Switch) obj;

            return positionOne.equals(otherSwitch.positionOne) && positionTwo.equals(otherSwitch.positionTwo);
        }

        return false;
    }

    @Override
    public int hashCode() {
        // consistent with equals: ignores current configuration
        return Objects.hash(positionOne, positionTwo);
    }
}
