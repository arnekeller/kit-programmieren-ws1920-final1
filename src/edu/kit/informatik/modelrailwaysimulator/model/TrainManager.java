package edu.kit.informatik.modelrailwaysimulator.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.SortedMap;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * Train manager, used for processing train placements and movement on a rail network.
 *
 * @author Arne Keller
 * @version 1.0
 */
public final class TrainManager {
    /**
     * Railway network used in this simulation.
     */
    private final RailwayNetwork railNetwork;
    /**
     * Map of trains simulated. The train identifier is used as key.
     */
    private final Map<Integer, Train> trains = new TreeMap<>();

    /**
     * Construct a new train manager that will operate on the provided rail network.
     *
     * @param railNetwork rail network to use
     */
    public TrainManager(RailwayNetwork railNetwork) {
        this.railNetwork = railNetwork;
    }

    /**
     * Check whether a train is on the specified rail. Trains only touching the end of the rail will also be considered.
     *
     * @param rail the rail to check
     * @return whether a train is on that rail
     */
    public boolean anyTrainOnRail(Rail rail) {
        return trains.values().stream().anyMatch(train -> train.isOnRail(rail));
    }

    /**
     * Remove any trains on the specified rail.
     *
     * @param rail rail to clear
     */
    public void removeTrainsOnRail(Rail rail) {
        trains.values().stream().filter(train -> train.isOnRail(rail)).forEach(Train::removeFromRails);
    }

    /**
     * Get the train containing the specified rolling stock.
     *
     * @param rollingStock rolling stock to search for
     * @return the train containing the rolling stock
     */
    public Optional<Train> getTrainContainingRollingStock(RollingStock rollingStock) {
        return trains.values().stream().filter(train -> train.contains(rollingStock)).findAny();
    }

    /**
     * Add a rolling stock to an existing train or create a new one.
     *
     * @param trainId train identifier
     * @param rollingStock rolling stock to add
     * @throws LogicException on invalid user input (e.g. rolling stock in use)
     */
    public void addTrain(int trainId, RollingStock rollingStock) throws LogicException {
        if (getTrainContainingRollingStock(rollingStock).isPresent()) {
            throw new LogicException("rolling stock already used");
        }
        final Train train = trains.get(trainId);
        if (train == null) { // create new train
            final int correctId = getNextTrainIdentifier();
            if (trainId != correctId) {
                throw new LogicException("new train identifier must be next free identifier");
            }
            final Train newTrain = new Train(trainId, rollingStock);
            trains.put(trainId, newTrain);
        } else {
            if (train.isPlaced()) {
                throw new LogicException("can not add rolling stock to placed train");
            }
            train.add(rollingStock);
        }
    }

    /**
     * Calculate the next available train identifier. Will fill 'holes' in the identifier list first.
     *
     * @return the next train identifier, or -1 if none available
     */
    private int getNextTrainIdentifier() {
        return IntStream.rangeClosed(1, Integer.MAX_VALUE)
                .filter(id -> !trains.containsKey(id)).findFirst().orElse(-1);
    }

    /**
     * Delete a train. Will not delete rolling stock contained in that train.
     *
     * @param trainId identifier of the train
     * @return whether the train could be deleted (false if not found)
     */
    public boolean deleteTrain(int trainId) {
        final Train train = trains.get(trainId);
        if (train != null) {
            trains.remove(trainId);
            return true;
        }
        return false;
    }

    /**
     * Get the trains (their text representation) in the simulation.
     *
     * @return sorted collection of trains
     */
    public SortedMap<Integer, String> getTrains() {
        return trains.values().stream()
                .collect(Collectors.toMap(Train::getIdentifier, Object::toString, (id1, id2) -> id1, TreeMap::new));
    }

    /**
     * Get the ASCII art representation of a train.
     *
     * @param trainId identifier of the train to show
     * @return ASCII art representation of said train
     * @throws LogicException if train not found
     */
    public List<String> showTrain(int trainId) throws LogicException {
        final Train train = trains.get(trainId);
        if (train != null) {
            return train.show();
        } else {
            throw new LogicException("no such train");
        }
    }

    /**
     * Attempt to place a train on the rail network.
     * The initial direction has to be parallel to one of the coordinate axes.
     *
     * @param trainId identifier of the train to place
     * @param position where to place the train
     * @param direction direction in which the train should initially go
     * @return whether the train was successfully placed
     * @throws LogicException if train could not be found, is already placed, is not valid, direction is invalid
     *  or railway network is not ready for trains
     */
    public boolean putTrain(int trainId, Vector2D position, Vector2D direction) throws LogicException {
        final Train train = trains.get(trainId);
        if (train == null) {
            throw new LogicException("train not found");
        } else if (!train.isProperTrain()) {
            throw new LogicException("train is not valid");
        } else if (train.isPlaced()) {
            throw new LogicException("train is already placed");
        } else if (!direction.isDirection()) {
            throw new LogicException("invalid train direction");
        } else if (!railNetwork.isReadyForTrains()) {
            throw new LogicException("switches not set up");
        }
        // attempt to place train
        final boolean placed = train.placeOn(railNetwork, position, direction);
        // check for collisions with existing trains
        if (placed && !getPlacementCollisions().isEmpty()) {
            train.removeFromRails();
            return false;
        } else {
            return placed;
        }
    }

    /**
     * Get collisions of trains currently placed. This implements the first collision checking algorithm (1A):
     * trains collide if they partially occupy the same rail or touch each other.
     * The specified list can contain derailed trains that could potentially be involved in crashes.
     *
     * @param collisions list of collisions to expand
     */
    private void getStaticCollisions(List<SortedSet<Train>> collisions) {
        final int maxId = trains.keySet().stream().max(Integer::compareTo).orElse(0);
        for (int id = 1; id <= maxId; id++) {
            final Train train = trains.get(id);
            if (train == null || !train.isPlaced()) {
                continue;
            }
            final Set<Rail> occupiedRails = train.getOccupiedRails();
            // check for same position, and rail collisions
            final SortedSet<Train> collision = IntStream.rangeClosed(id + 1, maxId)
                    .filter(trains::containsKey)
                    .mapToObj(trains::get)
                    .filter(Train::isPlaced)
                    .filter(otherTrain -> train.touches(otherTrain)
                            || otherTrain.getOccupiedRails().stream().anyMatch(occupiedRails::contains))
                    .collect(Collectors.toCollection(TreeSet::new));
            if (!collision.isEmpty()) {
                collision.add(train);
                addToSetOrAddNew(collisions, collision);
            }
        }
    }

    /**
     * Implementation of the silly second collision checking algorithm (2B):
     * two trains collide if they touch the same rail (!) or position.
     * Is strictly an extension of {@link #getStaticCollisions(List) getStaticCollisions}.
     *
     * @return list of collisions
     */
    private List<SortedSet<Train>> getPlacementCollisions() {
        final List<SortedSet<Train>> collisions = new ArrayList<>();
        trains.values().stream().filter(Train::isPlaced).forEach(train1 ->
                trains.values().stream().filter(train2 -> train2 != train1).filter(Train::isPlaced).forEach(train2 -> {
                    final Set<Rail> occupiedByTrain1 = train1.getOccupiedRails();
                    Collections.addAll(occupiedByTrain1, railNetwork.findTouchingRails(train1.getFrontPosition()));
                    Collections.addAll(occupiedByTrain1, railNetwork.findTouchingRails(train1.getBackPosition()));
                    final Set<Rail> occupiedByTrain2 = train2.getOccupiedRails();
                    Collections.addAll(occupiedByTrain2, railNetwork.findTouchingRails(train2.getFrontPosition()));
                    Collections.addAll(occupiedByTrain2, railNetwork.findTouchingRails(train2.getBackPosition()));
                    // calculate intersection
                    occupiedByTrain2.retainAll(occupiedByTrain1);
                    if (!occupiedByTrain2.isEmpty()) {
                        final SortedSet<Train> collision
                                = Stream.of(train1, train2).collect(Collectors.toCollection(TreeSet::new));
                        addToSetOrAddNew(collisions, collision);
                    }
                }));
        return collisions;
    }

    /**
     * Add the set to an existing set (and merge sets), or add it to the list.
     */
    private <T> void addToSetOrAddNew(List<SortedSet<T>> setList, SortedSet<T> newSet) {
        Set<T> existing = null;
        int i = 0;
        while (i < setList.size()) {
            final Set<T> otherSet = setList.get(i);
            if (otherSet.stream().anyMatch(newSet::contains)) {
                // other set overlaps with new set
                if (existing == null) {
                    existing = otherSet;
                    existing.addAll(newSet);
                } else {
                    // remove overlapping sets and merge into a single set
                    existing.addAll(otherSet);
                    setList.remove(i);
                    continue;
                }
            }
            i++;
        }
        if (existing == null) {
            // add set to the list if no overlap was found
            setList.add(newSet);
        }
    }

    /**
     * Get the front position of the specified train.
     *
     * @param trainId train identifier
     * @return front position of that train
     */
    public Vector2D getPosition(int trainId) {
        return trains.get(trainId).getFrontPosition();
    }

    /**
     * Get collisions of moving the trains one step forward, removing crashing trains from the rails in the process.
     *
     * @return list of collisions (never null, sometimes empty)
     */
    private List<SortedSet<Train>> getCollisionsOfOneStep(Function<Train, Vector2D> movementFunction) {
        final List<SortedSet<Train>> collisions = new ArrayList<>();
        trains.values().stream().filter(Train::isPlaced).forEach(train -> {
            final Vector2D nextPosition = movementFunction.apply(train);
            if (nextPosition == null) {
                // train is derailing
                collisions.add(Stream.of(train).collect(Collectors.toCollection(TreeSet::new)));
            }
        });
        getStaticCollisions(collisions);
        collisions.forEach(collision -> collision.forEach(Train::removeFromRails));
        return collisions;
    }

    /**
     * Move the trains in this simulation.
     *
     * @param speed amount of steps to move the trains
     * @return train collisions, no value if no trains are placed
     * @throws LogicException if simulation is not yet ready
     */
    public Optional<List<SortedSet<Integer>>> step(short speed) throws LogicException {
        if (!railNetwork.isReadyForTrains()) {
            throw new LogicException("rail tracks/switches not set up");
        }
        if (trains.values().stream().noneMatch(Train::isPlaced)) {
            return Optional.empty();
        }

        return Optional.of(IntStream.range(0, Math.abs(speed))
                .mapToObj(step -> speed >= 0
                        ? getCollisionsOfOneStep(train -> train.stepForward(railNetwork))
                        : getCollisionsOfOneStep(train -> train.stepBackward(railNetwork)))
                // replace train references with identifiers to prevent train modification by caller
                .flatMap(collisions -> collisions.stream()
                        .map(collision -> collision.stream().map(Train::getIdentifier)
                                .collect(Collectors.toCollection(TreeSet::new))))
                // sort by train with lowest identifier in crash
                .sorted(Comparator.comparing(TreeSet::first))
                .collect(Collectors.toList()));
    }
}
