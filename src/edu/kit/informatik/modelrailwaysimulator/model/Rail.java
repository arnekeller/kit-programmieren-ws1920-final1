package edu.kit.informatik.modelrailwaysimulator.model;

/**
 * Generic rail that other rails can connect to.
 *
 * @author Arne Keller
 * @version 1.2
 */
public abstract class Rail {
    /**
     * Unique positive identifier of this rail.
     */
    private final int id;

    /**
     * Initialize a new rail with the specified identifier.
     *
     * @param id the identifier of this rail
     * @throws IllegalArgumentException if the id is not positive
     */
    protected Rail(int id) {
        if (id < 1) {
            throw new IllegalArgumentException("rail id has to be positive!");
        }
        this.id = id;
    }

    /**
     * @return the unique identifier of this rail
     */
    public int getIdentifier() {
        return this.id;
    }

    /**
     * Calculate the (Manhattan) length of this rail.
     *
     * @return the length of this rail
     */
    public abstract long getLength();

    /**
     * Check whether this rail connects to a specified position. Note that this will return false if the rail only
     * contains the position!
     *
     * @param point point to check for connection
     * @return whether the rail currently connects to the point
     */
    public abstract boolean connectsTo(Vector2D point);

    /**
     * Check whether this rail can connect to a specified position. Only relevant for rails with switching capabilities.
     * Note that this will return false if the rail only contains the position!
     *
     * @param point point to check for connection
     * @return whether the rail can connect to the point
     */
    public abstract boolean canConnectTo(Vector2D point);

    /**
     * Check whether this rail can connect to another rail.
     *
     * @param rail rail to check for possible connection
     * @return whether this rail can connect to the specified rail
     */
    public abstract boolean canConnectToRail(Rail rail);

    /**
     * Check whether this rail contains a position. Note that this will return false if the rail only touches the
     * position.
     *
     * @param position the point to check
     * @return whether the point is inside this rail (not on the edge)
     */
    public abstract boolean contains(Vector2D position);

    /**
     * Try to set the rail to connect to this position. Obviously only makes sense for switches and similar rails.
     *
     * @param position point to connect to
     * @return whether rail could be successfully set
     */
    public boolean switchTo(Vector2D position) {
        return false;
    }

    /**
     * Get the direction trains can move on this rail starting at the specified position.
     * Returns only one direction since movements have to be deterministic.
     *
     * @param position the position to check
     * @return the direction trains can move starting at that position
     */
    public abstract Vector2D getDirectionFrom(Vector2D position);

    /**
     * Check whether a train can be placed on this rail facing the specified direction.
     *
     * @param position train front position
     * @param direction the normalized direction vector to check
     * @return whether a train can be placed on this rail facing that direction
     */
    public abstract boolean allowsPlacement(Vector2D position, Vector2D direction);

    /**
     * Check whether a train can be move on this rail in the specified direction.
     *
     * @param position start position
     * @param direction the normalized direction vector to check
     * @return whether a train can move on this rail in the direction
     */
    public abstract boolean allowsMovement(Vector2D position, Vector2D direction);

    /**
     * Move a position along this rail in the specified direction for the specified amount of steps. This method will
     * stop at the end of the rail, even if that will omit some steps.
     *
     * @param position position to move from
     * @param direction direction to go
     * @param steps amount of steps
     * @return moved position
     */
    public abstract Vector2D move(Vector2D position, Vector2D direction, long steps);

    /**
     * Check whether this rail is ready for usage by trains.
     *
     * @return whether the rail is ready for trains running on it
     */
    public abstract boolean isReadyForTrains();
}
