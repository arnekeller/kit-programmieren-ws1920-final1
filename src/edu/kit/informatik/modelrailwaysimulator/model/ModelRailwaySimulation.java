package edu.kit.informatik.modelrailwaysimulator.model;

import edu.kit.informatik.modelrailwaysimulator.ui.CoachType;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.SortedMap;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Model railway simulation. Can simulate trains moving on a railway network. Features:
 * <ul>
 *     <li>allows creating different types of rolling stock: engines, train-sets and coaches</li>
 *     <li>handles both straight tracks and switches</li>
 *     <li>only supports deterministic rail networks</li>
 *     <li>trains can be composed of many rolling stocks</li>
 *     <li>fancy ASCII art of trains can be generated</li>
 *     <li>collision checking is slightly simplified and operates on a rail-based model</li>
 *     <li>large railway networks can be simulated quickly</li>
 * </ul>
 *
 * @author Arne Keller
 * @version 1.0
 */
public class ModelRailwaySimulation {
    /**
     * Railway network used in this simulation.
     */
    private final RailwayNetwork railNetwork = new RailwayNetwork();
    /**
     * Train manager used to simulate train placement and movement.
     */
    private final TrainManager trainManager = new TrainManager(railNetwork);
    /**
     * Collection of engines used in the simulation keyed by their identifier.
     */
    private final Map<String, Engine> engines = new TreeMap<>();
    /**
     * Collection of train sets used in the simulation keyed by their identifier.
     */
    private final Map<String, TrainSet> trainSets = new TreeMap<>();
    /**
     * Map of coaches used in the simulation.
     */
    private final Map<Integer, Coach> coaches = new TreeMap<>();

    /**
     * Add a new track to the rail network of this simulation.
     *
     * @param start start position of the new track
     * @param end end position of the new track
     * @return the positive identifier of the new track if successful, -1 if none available
     * @throws LogicException if user input is invalid (e.g. zero length track)
     */
    public int addTrack(Vector2D start, Vector2D end) throws LogicException {
        return railNetwork.addTrack(start, end);
    }

    /**
     * Add a switch to the rail network of this simulation.
     *
     * @param start start point of the switch
     * @param end1 end position 1 of the switch
     * @param end2 end position 2 of the switch
     * @return the positive identifier of the switch if successful, -1 otherwise
     * @throws LogicException if the new switch would be invalid
     */
    public int addSwitch(Vector2D start, Vector2D end1, Vector2D end2) throws LogicException {
        return railNetwork.addSwitch(start, end1, end2);
    }

    /**
     * Remove the specified rail from the rail network.
     *
     * @param id identifier of the rail to remove
     * @return whether the rail could be successfully removed
     */
    public boolean removeRail(int id) {
        // check whether any trains are on this rail
        return !railNetwork.getRail(id).map(trainManager::anyTrainOnRail).orElse(true)
                && railNetwork.removeRail(id);
    }

    /**
     * Get a list of all rails in the network, specifying their identifier, start and end points, their length and
     * their current configuration.
     *
     * @return list of rails
     */
    public List<String> listTracks() {
        return railNetwork.listTracks();
    }

    /**
     * Change the setting of a switch in the rail network. Will derail trains on that switch.
     *
     * @param id identifier of the switch
     * @param position position to set the switch to
     * @return whether the switch could be set
     */
    public boolean setSwitch(int id, Vector2D position) {
        final boolean success = railNetwork.setSwitch(id, position);
        if (success) { // derail trains on switch, explicitly not (!) printing any removed trains (source: forum post)
            trainManager.removeTrainsOnRail(railNetwork.getRail(id).get());
        }
        return success;
    }

    /**
     * Add a new engine to the simulation.
     *
     * @param newEngine the engine to add to the simulation
     * @throws LogicException when the identifier is already in use
     */
    public void createEngine(Engine newEngine) throws LogicException {
        final String id = newEngine.getIdentifier();
        if (engines.containsKey(id) || trainSets.containsKey(id)) {
            throw new LogicException("engine identifier already used");
        }
        engines.put(id, newEngine);
    }

    /**
     * Get a list of engines (their text representation) added to the simulation.
     *
     * @return list of engines (never null, sometimes empty)
     */
    public List<String> listEngines() {
        return engines.values().stream().map(engine -> {
            final String trainId = trainManager.getTrainContainingRollingStock(engine)
                    .map(train -> Integer.toString(train.getIdentifier()))
                    .orElse("none");
            return String.format("%s %s", trainId, engine);
        }).collect(Collectors.toList());
    }

    /**
     * Add a new coach to the simulation.
     *
     * @param coachType type of the coach
     * @param length length of the coach
     * @param couplingFront whether the coach should have a front coupling
     * @param couplingBack whether the coach should have a back coupling
     * @return the identifier of the coach if successfully added, -1 if none available
     * @throws LogicException if user input is invalid (e.g. zero-sized coach)
     */
    public int createCoach(CoachType coachType, int length,
                           boolean couplingFront, boolean couplingBack) throws LogicException {
        final int id = getNextCoachIdentifier();
        if (id < 0) {
            return -1;
        }
        coaches.put(id, coachType.createCoach(id, length, couplingFront, couplingBack));
        return id;
    }

    /**
     * Calculate the next coach identifier.
     *
     * @return the next coach identifier, or -1 if none available
     */
    private int getNextCoachIdentifier() {
        return IntStream.rangeClosed(1, Integer.MAX_VALUE)
                .filter(id -> !coaches.containsKey(id)).findFirst().orElse(-1);
    }

    /**
     * Get a list of coaches (their text representation) added to the simulation.
     *
     * @return list of coaches (never null, sometimes empty)
     */
    public List<String> listCoaches() {
        return coaches.values().stream().map(coach -> {
            final String trainId = trainManager.getTrainContainingRollingStock(coach)
                    .map(train -> Integer.toString(train.getIdentifier()))
                    .orElse("none");
            return String.format("%d %s %s %d %b %b",
                    coach.getNumericalIdentifier(), trainId, coach.getType(),
                    coach.getLength(), coach.hasCouplingFront(), coach.hasCouplingBack());
        }).collect(Collectors.toList());
    }

    /**
     * Add a new train set to the simulation.
     *
     * @param newTrainSet the train set to add
     * @throws LogicException if the identifier is already used
     */
    public void createTrainSet(TrainSet newTrainSet) throws LogicException {
        final String id = newTrainSet.getIdentifier();
        if (engines.containsKey(id) || trainSets.containsKey(id)) {
            throw new LogicException("train set identifier already used");
        }
        trainSets.put(id, newTrainSet);
    }

    /**
     * Get a sorted list of train sets (their text representation) added to the simulation.
     *
     * @return list of train sets (never null, sometimes empty)
     */
    public List<String> listTrainSets() {
        return trainSets.values().stream().map(trainSet -> {
            String trainId = trainManager.getTrainContainingRollingStock(trainSet)
                    .map(train -> Integer.toString(train.getIdentifier()))
                    .orElse("none");
            return String.format("%s %s", trainId, trainSet);
        }).collect(Collectors.toList());
    }

    /**
     * Delete a rolling stock. Will not return rolling stock used in a train, you have to delete the train first!
     *
     * @param id identifier of the rolling stock to remove
     * @return whether the rolling was successfully removed
     */
    public boolean deleteRollingStock(String id) {
        final Optional<RollingStock> rollingStock = getRollingStock(id);
        if (!rollingStock.isPresent()) {
            return false; // can not remove imaginary rolling stock
        }
        if (trainManager.getTrainContainingRollingStock(rollingStock.get()).isPresent()) {
            return false; // can not delete rolling stock in use
        }
        // remove the rolling stock from one of the collections
        return engines.remove(rollingStock.get().getIdentifier()) != null
                || trainSets.remove(rollingStock.get().getIdentifier()) != null
                || coaches.values().remove(rollingStock.get());
    }

    /**
     * Find a rolling stock by identifier.
     *
     * @param id identifier of the rolling stock to find
     * @return the specified rolling stock
     */
    private Optional<RollingStock> getRollingStock(String id) {
        if (Coach.IDENTIFIER_PATTERN.matcher(id).matches()) {
            final int coachId = Integer.parseInt(id.substring(1));
            return Optional.ofNullable(coaches.get(coachId));
        } else {
            final Engine engine = engines.get(id);
            if (engine != null) {
                return Optional.of(engine);
            }
            return Optional.ofNullable(trainSets.get(id));
        }
    }

    /**
     * Create a new train or add rolling stock to an existing train.
     *
     * @param trainId identifier of the train
     * @param rollingStockId identifier of the rolling stock
     * @return the added rolling stock
     * @throws LogicException if train not found and train identifier is not next identifier or rolling stock
     *  is not found
     */
    public RollingStock addTrain(int trainId, String rollingStockId) throws LogicException {
        final Optional<RollingStock> rollingStock = getRollingStock(rollingStockId);
        if (!rollingStock.isPresent()) {
            throw new LogicException("rolling stock not found");
        }
        trainManager.addTrain(trainId, rollingStock.get());
        return rollingStock.get();
    }

    /**
     * Delete a train.
     *
     * @param id identifier of the train
     * @return whether the train could be deleted
     */
    public boolean deleteTrain(int id) {
        return trainManager.deleteTrain(id);
    }

    /**
     * Get the trains (their text representation) in the simulation.
     *
     * @return sorted collection of trains
     */
    public SortedMap<Integer, String> getTrains() {
        return trainManager.getTrains();
    }

    /**
     * Get the front position of a train.
     *
     * @param trainId train identifier
     * @return position of the train, or null if not found or placed
     */
    public Vector2D getTrainPosition(int trainId) {
        return trainManager.getPosition(trainId);
    }

    /**
     * Get the ASCII art representation of a train.
     *
     * @param id identifier of the train to show
     * @return rows of ASCII art representing this train
     * @throws LogicException if train not found
     */
    public List<String> showTrain(int id) throws LogicException {
        return trainManager.showTrain(id);
    }

    /**
     * Put a train on the rail network. The initial direction has to be parallel to one of the coordinate axes.
     *
     * @param trainId identifier of the train to place
     * @param position where to place the train
     * @param direction direction in which the train should initially go
     * @return whether the train was successfully placed
     * @throws LogicException if train could not be found, is already placed, is not valid, direction is invalid
     *  or railway network is not ready for trains
     */
    public boolean putTrain(int trainId, Vector2D position, Vector2D direction) throws LogicException {
        return trainManager.putTrain(trainId, position, direction);
    }

    /**
     * Move the trains in this simulation.
     *
     * @param speed amount of steps to move the trains
     * @return train collisions, no value if no trains are placed
     * @throws LogicException if the simulation is not yet ready
     */
    public Optional<List<SortedSet<Integer>>> step(short speed) throws LogicException {
        return trainManager.step(speed);
    }
}