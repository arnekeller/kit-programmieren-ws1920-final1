package edu.kit.informatik.modelrailwaysimulator.model;

/**
 * A freight coach.
 *
 * @author Arne Keller
 * @version 1.0
 */
public class FreightCoach extends Coach {
    /**
     * ASCII art representation of a freight coach.
     */
    private static final String[] FREIGHT_TEXT = new String[]{
        "|                  |",
        "|                  |",
        "|                  |",
        "|__________________|",
        "   (O)        (O)   "
    };

    /**
     * Construct a new freight coach.
     *
     * @param identifier identifier to use
     * @param length length of coach
     * @param couplingFront whether the coach should have a front coupling
     * @param couplingBack whether the coach should have a back coupling
     * @throws LogicException on invalid user input (e.g. zero-sized coach)
     */
    public FreightCoach(int identifier, int length, boolean couplingFront, boolean couplingBack)
            throws LogicException {
        super(identifier, length, couplingFront, couplingBack);
    }

    @Override
    public String description() {
        return "freight coach";
    }

    @Override
    public String[] textRepresentation() {
        // make sure caller can not modify private data
        return FREIGHT_TEXT.clone();
    }

    @Override
    public String getType() {
        return "f";
    }
}
