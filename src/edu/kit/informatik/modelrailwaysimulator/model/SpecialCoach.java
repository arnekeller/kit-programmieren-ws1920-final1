package edu.kit.informatik.modelrailwaysimulator.model;

/**
 * A special coach, used for e.g. firefighting.
 *
 * @author Arne Keller
 * @version 1.0
 */
public class SpecialCoach extends Coach {
    /**
     * ASCII art representation of a special coach.
     */
    private static final String[] SPECIAL_TEXT = new String[]{
        "               ____",
        "/--------------|  |",
        "\\--------------|  |",
        "  | |          |  |",
        " _|_|__________|  |",
        "|_________________|",
        "   (O)       (O)   "
    };

    /**
     * Construct a new special coach.
     *
     * @param identifier identifier to use
     * @param length length of coach
     * @param couplingFront whether the coach should have a front coupling
     * @param couplingBack whether the coach should have a back coupling
     * @throws LogicException on invalid user input (e.g. zero-sized coach)
     */
    public SpecialCoach(int identifier, int length, boolean couplingFront, boolean couplingBack)
            throws LogicException {
        super(identifier, length, couplingFront, couplingBack);
    }

    @Override
    public String description() {
        return "special coach";
    }

    @Override
    public String[] textRepresentation() {
        // make sure caller can not modify private data
        return SPECIAL_TEXT.clone();
    }

    @Override
    public String getType() {
        return "s";
    }
}
