package edu.kit.informatik.modelrailwaysimulator.model;

import java.util.regex.Pattern;

import static edu.kit.informatik.modelrailwaysimulator.ui.command.CommandFactory.NUMBER;

/**
 * A coach.
 *
 * @author Arne Keller
 * @version 1.0
 */
public abstract class Coach extends RollingStock {
    /**
     * String used to prefix coach identifiers.
     */
    public static final String IDENTIFIER_PREFIX = "W";
    /**
     * Pattern to match a single coach identifier.
     */
    public static final Pattern IDENTIFIER_PATTERN = Pattern.compile(IDENTIFIER_PREFIX + NUMBER);

    /**
     * The unique positive identifier of this coach.
     */
    private final int identifier;

    /**
     * Construct a new coach.
     *
     * @param identifier identifier to use
     * @param length length of coach
     * @param couplingFront whether the coach should have a front coupling
     * @param couplingBack whether the coach should have a back coupling
     * @throws LogicException on invalid user input (e.g. zero-sized coach)
     */
    public Coach(int identifier, int length, boolean couplingFront, boolean couplingBack) throws LogicException {
        super(length, couplingFront, couplingBack);
        if (identifier < 1) {
            throw new LogicException("coach id has to be positive!");
        }
        this.identifier = identifier;
    }

    /**
     * Get the textual identifier of this coach in this format: [PREFIX][NUMERICAL_IDENTIFIER].
     *
     * @return the textual identifier of this coach
     */
    @Override
    public String getIdentifier() {
        return String.format("%s%d", IDENTIFIER_PREFIX, identifier);
    }

    @Override
    public boolean canCoupleTo(RollingStock rollingStock) {
        return true;
    }

    @Override
    public boolean usefulAtEndOfTrain() {
        return false;
    }

    /**
     * @return the numerical identifier of this coach
     */
    public int getNumericalIdentifier() {
        return identifier;
    }

    /**
     * Get the type of this coach ("p" for passenger, "f" for freight, "s" for special).
     *
     * @return abbreviation of the type of this coach
     */
    public abstract String getType();
}
