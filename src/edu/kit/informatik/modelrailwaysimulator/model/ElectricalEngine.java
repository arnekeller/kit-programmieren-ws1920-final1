package edu.kit.informatik.modelrailwaysimulator.model;

/**
 * Electrical engine.
 *
 * @author Arne Keller
 * @version 1.0
 */
public class ElectricalEngine extends Engine {
    /**
     * ASCII art of an electrical engine.
     */
    private static final String[] ELECTRICAL_ENGINE_TEXT = new String[]{
        "               ___    ",
        "                 \\    ",
        "  _______________/__  ",
        " /_| ____________ |_\\ ",
        "/   |____________|   \\",
        "\\                    /",
        " \\__________________/ ",
        "  (O)(O)      (O)(O)  ",
    };

    /**
     * Construct a new electrical engine.
     *
     * @param series series/class of engine
     * @param name name of engine
     * @param length length of engine
     * @param couplingFront whether the engine should have a front coupling
     * @param couplingBack whether the engine should have a back coupling
     * @throws LogicException on invalid user input (e.g. zero-sized engine)
     */
    public ElectricalEngine(String series, String name, int length, boolean couplingFront, boolean couplingBack)
            throws LogicException {
        super(series, name, length, couplingFront, couplingBack);
    }

    @Override
    public String getType() {
        return "e";
    }

    @Override
    public String[] textRepresentation() {
        // make sure caller can not modify private data
        return ELECTRICAL_ENGINE_TEXT.clone();
    }

    @Override
    public String description() {
        return "electrical engine";
    }
}
