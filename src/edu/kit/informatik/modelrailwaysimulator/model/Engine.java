package edu.kit.informatik.modelrailwaysimulator.model;

/**
 * Generic engine, is usually either diesel, steam or electric.
 *
 * @author Arne Keller
 * @version 1.0
 */
public abstract class Engine extends RollingStock {
    /**
     * Series/class of this engine.
     */
    private final String series;
    /**
     * Name of this engine.
     */
    private final String name;

    /**
     * Initialize an engine.
     *
     * @param series series/class of this engine
     * @param name name of this engine
     * @param length length of this engine
     * @param couplingFront whether this engine should have a front coupling
     * @param couplingBack whether this engine should have a back coupling
     * @throws LogicException on invalid user input (zero-sized coach)
     */
    protected Engine(String series, String name, int length,
                     boolean couplingFront, boolean couplingBack) throws LogicException {
        super(length, couplingFront, couplingBack);
        this.series = series;
        this.name = name;
    }

    /**
     * @return the name of this engine
     */
    public String getName() {
        return name;
    }

    /**
     * @return the series of this engine
     */
    public String getSeries() {
        return series;
    }

    @Override
    public String getIdentifier() {
        return String.format("%s-%s", series, name);
    }

    @Override
    public boolean canCoupleTo(RollingStock rollingStock) {
        return true;
    }

    @Override
    public boolean usefulAtEndOfTrain() {
        return true;
    }

    /**
     * Get the type of this engine ("s" for steam, "d" for diesel, "e" for electrical).
     *
     * @return abbreviation of the type of this engine
     */
    public abstract String getType();

    /**
     * Get the textual representation of this engine in the format:
     * [type] [series] [name] [length] [couplingFront] [couplingBack]
     *
     * @return textual representation of the engine
     */
    @Override
    public String toString() {
        return String.format("%s %s %s %d %b %b", getType(), series, name, getLength(),
                hasCouplingFront(), hasCouplingBack());
    }
}
