package edu.kit.informatik.modelrailwaysimulator.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * A train consisting of one or more rolling stock. Can be placed on a rail network.
 *
 * @author Arne Keller
 * @version 1.1
 */
public final class Train implements Comparable<Train> {
    /**
     * Separator between rolling stocks in {@link #show()} and {@link #toString()}.
     */
    private static final char ROLLING_STOCK_SEPARATOR = ' ';

    /**
     * Numerical identifier of this train.
     */
    private final int identifier;
    /**
     * List of rolling stocks in this train.
     */
    private final List<RollingStock> rollingStocks = new ArrayList<>();
    /**
     * List of positions this train occupies. More specifically, it only contains:
     * the position of the front of the train, any positions on rail connections, the position of the back of the train
     * <p>
     * This is a linked list because we only have to update
     * the positions at the start and end of the list when moving the train.
     */
    private LinkedList<Vector2D> positions;
    /**
     * Set of rails this train occupies.
     */
    private Set<Rail> occupiedRails;

    /**
     * Construct a new train.
     *
     * @param identifier identifier to use
     * @param rollingStock initial rolling stock
     */
    public Train(int identifier, RollingStock rollingStock) {
        this.identifier = identifier;
        this.rollingStocks.add(rollingStock);
    }

    /**
     * @return the identifier of this train
     */
    public int getIdentifier() {
        return identifier;
    }

    /**
     * Check whether a rolling stock is used in this train.
     *
     * @param rollingStock rolling stock to check
     * @return whether the specified rolling stock is in this train
     */
    public boolean contains(RollingStock rollingStock) {
        return rollingStocks.contains(rollingStock);
    }

    /**
     * Add a rolling stock to this train.
     *
     * @param rollingStock the rolling stack to add
     * @throws LogicException if the rolling stock could not be added to the train
     */
    public void add(RollingStock rollingStock) throws LogicException {
        final RollingStock last = rollingStocks.get(rollingStocks.size() - 1);
        // make sure special coupling requirements are honored
        if (rollingStock.hasCouplingFront() && last.hasCouplingBack()
                && rollingStock.canCoupleTo(last) && last.canCoupleTo(rollingStock)) {
            rollingStocks.add(rollingStock);
        } else {
            throw new LogicException("could not add rolling stock to train");
        }
    }

    /**
     * Check whether this train is on a particular rail. Just touching the rail is enough.
     *
     * @param rail the rail to check
     * @return whether this train is on that rail
     */
    public boolean isOnRail(Rail rail) {
        return isPlaced()
                && (occupiedRails.stream().anyMatch(blockedRail -> blockedRail == rail)
                || positions.stream().anyMatch(rail::canConnectTo));
    }

    /**
     * Put a train on the rail network.
     *
     * @param railNetwork rail network to place the train on
     * @param frontPosition where to place the train
     * @param rawDirection direction in which the train should initially go
     * @return whether the train was successfully placed
     */
    public boolean placeOn(RailwayNetwork railNetwork, Vector2D frontPosition, Vector2D rawDirection) {
        if (isPlaced()) {
            return false; // do not place a train that is already placed
        }
        final Vector2D direction = rawDirection.normalized();
        // check that the requested direction is equal to the direction of one the tracks
        if (!railNetwork.allowsPlacementAt(frontPosition, direction)) {
            return false;
        }
        Vector2D positioningDirection = direction.negated();
        // size of train that still needs to be placed
        long length = getLength();

        final LinkedList<Vector2D> positionList = new LinkedList<>();
        positionList.addLast(frontPosition);
        Vector2D position = frontPosition;
        final Set<Rail> occupiedRailsSet = new HashSet<>();
        while (length > 0) { // while part of the train still needs to be placed
            // move alongside rail network
            position = railNetwork.move(position, positioningDirection, length);
            if (position == null) {
                // derailed
                return false;
            }
            final long distanceToLastPosition = positionList.getLast().distanceTo(position);
            if (distanceToLastPosition == 0) {
                // stuck at end of rail
                return false;
            }

            // successfully placed part of the train
            length -= distanceToLastPosition;
            // update direction
            positioningDirection = position.subtract(positionList.getLast()).normalized();

            final Rail occupiedRail = railNetwork.findRail(positionList.getLast(), position).get();
            if (occupiedRailsSet.contains(occupiedRail) && positionList.size() > 4) {
                // perhaps a self intersection
                final long occupiedAtFrontOfTrain = positionList.getFirst().distanceTo(positionList.get(1));
                final long occupiedAtBackOfTrain = positionList.getLast()
                        .distanceTo(positionList.get(positionList.size() - 2));
                if (occupiedAtFrontOfTrain + occupiedAtBackOfTrain > occupiedRail.getLength()) {
                    // train definitely intersects itself
                    return false;
                }
            }
            occupiedRailsSet.add(occupiedRail);
            positionList.addLast(position);
        }

        this.positions = positionList;
        this.occupiedRails = occupiedRailsSet;

        return true;
    }

    /**
     * @return whether this train is placed on a rail network
     */
    public boolean isPlaced() {
        return positions != null && occupiedRails != null;
    }

    /**
     * Check whether this train is correctly set up and ready to roll. That means at least one of the rolling stocks at
     * the front and back has to be useful.
     *
     * @return whether this train is valid
     */
    public boolean isProperTrain() {
        if (rollingStocks.isEmpty()) {
            return false;
        }
        final RollingStock first = rollingStocks.get(0);
        final RollingStock last = rollingStocks.get(rollingStocks.size() - 1);
        return first.usefulAtEndOfTrain() || last.usefulAtEndOfTrain();
    }

    /**
     * @return position of the front of the train
     */
    public Vector2D getFrontPosition() {
        return positions != null ? positions.getFirst() : null;
    }

    /**
     * @return position of the back of the train
     */
    public Vector2D getBackPosition() {
        return positions != null ? positions.getLast() : null;
    }

    /**
     * Compute the direction vector of this train.
     *
     * @return the direction of this train when moving forward
     */
    public Vector2D getDirection() {
        return positions.get(0).subtract(positions.get(1)).normalized();
    }

    /**
     * Compute the direction vector of the back of the train.
     *
     * @return the direction of this train when moving backward
     */
    public Vector2D getBackDirection() {
        return positions.getLast().subtract(positions.get(positions.size() - 2)).normalized();
    }

    /**
     * Get the rails this train currently occupies a non-empty part of. Simply touching a point of another rail will
     * not include that rail in this set.
     *
     * @return a set of the rails this train occupies
     */
    public Set<Rail> getOccupiedRails() {
        // need a copy (not a view) because the set will be modified when moving,
        // but caller expects *current* state, not some weird auto-updating set
        return new HashSet<>(occupiedRails);
    }

    /**
     * Move this train one step forward.
     *
     * @param railNetwork railway network to move on
     * @return new position, null if derailing
     */
    public Vector2D stepForward(RailwayNetwork railNetwork) {
        final Vector2D position = getFrontPosition();
        final Vector2D direction = getDirection();
        final Vector2D nextPosition = railNetwork.move(position, direction, 1);
        if (nextPosition == null || nextPosition.equals(position)) {
            // train is derailing
            moveTo(railNetwork, null);
            return null;
        } else {
            // train is moving successfully
            moveTo(railNetwork, nextPosition);
            return nextPosition;
        }
    }

    /**
     * Move this train one step backward.
     *
     * @param railNetwork railway network to move on
     * @return new position, null if derailing
     */
    public Vector2D stepBackward(RailwayNetwork railNetwork) {
        final Vector2D position = getBackPosition();
        final Vector2D direction = getBackDirection();
        final Vector2D nextPosition = railNetwork.move(position, direction, 1);
        if (nextPosition == null || nextPosition.equals(position)) {
            // derailing
            moveBackTo(railNetwork, null);
            return null;
        } else {
            // train moving successfully
            moveBackTo(railNetwork, nextPosition);
            return nextPosition;
        }
    }

    /**
     * Move this train on a train network to the specified position. Will not remove itself from rails if derailing.
     *
     * @param railNetwork rail network to move train on
     * @param nextPosition position to move train to
     */
    private void moveTo(RailwayNetwork railNetwork, Vector2D nextPosition) {
        final Optional<Rail> railUnderBackOfTrain = railNetwork.findContainingRail(positions.getLast());
        positions.set(positions.size() - 1, positions.getLast().subtract(getBackDirection()));
        if (positions.getLast().equals(positions.get(positions.size() - 2))) { // left a rail behind
            occupiedRails.remove(railUnderBackOfTrain.orElse(null));
            positions.removeLast();
        }
        if (nextPosition != null) {
            // not derailing
            positions.addFirst(nextPosition);
            simplifyPositions(railNetwork);
            occupiedRails.add(railNetwork.findRail(positions.get(1), nextPosition).get());
        } // else: derailing
    }

    /**
     * Move this train on a train network backwards to the specified position. Will not remove itself from rails if
     * derailing.
     *
     * @param railNetwork rail network to move train on
     * @param backPosition position to move back of the train to
     */
    private void moveBackTo(RailwayNetwork railNetwork, Vector2D backPosition) {
        // a common method for both directions would be a huge mess: requires at least six (!) extra parameters
        final Optional<Rail> railUnderFrontOfTrain = railNetwork.findContainingRail(positions.getFirst());
        positions.set(0, positions.getFirst().subtract(getDirection()));
        if (positions.getFirst().equals(positions.get(1))) { // left a rail behind
            occupiedRails.remove(railUnderFrontOfTrain.orElse(null));
            positions.removeFirst();
        }
        if (backPosition != null) {
            // not derailing
            positions.addLast(backPosition);
            simplifyPositions(railNetwork);
            occupiedRails.add(railNetwork.findRail(positions.get(positions.size() - 2), backPosition).get());
        } // else: derailing
    }

    /**
     * Simplify internal position data.
     * Removes positions that are contained between two other positions at the front or back of the train.
     *
     * @param railNetwork railway network to use
     */
    private void simplifyPositions(RailwayNetwork railNetwork) {
        if (positions.size() >= 3
                && positions.getFirst().subtract(positions.get(1)).normalized()
                .equals(positions.get(1).subtract(positions.get(2)).normalized())
                && railNetwork.findTouchingRails(positions.get(1)).length == 0) {
            positions.remove(1);
        }
        if (positions.size() >= 3
                && positions.getLast().subtract(positions.get(positions.size() - 2)).normalized()
                .equals(positions.get(positions.size() - 2)
                        .subtract(positions.get(positions.size() - 3)).normalized())
                && railNetwork.findTouchingRails(positions.get(positions.size() - 2)).length == 0) {
            positions.remove(positions.size() - 2);
        }
    }

    /**
     * Remove this train from the rail network.
     */
    public void removeFromRails() {
        positions = null;
        occupiedRails = null;
    }

    /**
     * Check whether this train touches another train.
     * Two trains ends occupying the same point is also considered touching.
     *
     * @param other other train
     * @return whether this train touches the other train
     */
    public boolean touches(Train other) {
        return positions != null && other.isOnAnyPosition(positions);
    }

    /**
     * Check if this train is on any of the provided positions. This includes the front and back of the train.
     *
     * @param positions collection of positions to check
     * @return whether this train is on any of the specified positions
     */
    public boolean isOnAnyPosition(Collection<Vector2D> positions) {
        return this.positions != null && this.positions.stream().anyMatch(positions::contains);
    }

    /**
     * Calculate the length of this train, summing the length of all rolling stocks.
     *
     * @return total length of this train
     */
    public long getLength() {
        return rollingStocks.stream().mapToLong(RollingStock::getLength).sum();
    }

    /**
     * Get the ASCII art of this train.
     *
     * @return rows of ASCII art
     */
    public List<String> show() {
        final List<StringBuilder> lines = new ArrayList<>();
        for (final RollingStock rollingStock : rollingStocks) {
            for (final StringBuilder line : lines) {
                line.append(ROLLING_STOCK_SEPARATOR);
            }
            final String[] text = rollingStock.textRepresentation();
            final int currentLength = lines.stream().mapToInt(StringBuilder::length).max().orElse(0);
            while (lines.size() < text.length) {
                final StringBuilder line = new StringBuilder();
                for (int i = 0; i < currentLength; i++) {
                    line.append(ROLLING_STOCK_SEPARATOR);
                }
                lines.add(0, line);
            }
            for (int i = 0; i < text.length; i++) {
                lines.get(lines.size() - 1 - i).append(text[text.length - 1 - i]);
            }
            final int finalLength = lines.stream().mapToInt(StringBuilder::length).max().getAsInt();
            for (final StringBuilder line : lines) {
                while (line.length() < finalLength) {
                    line.append(ROLLING_STOCK_SEPARATOR);
                }
            }
        }
        return lines.stream().map(StringBuilder::toString).collect(Collectors.toList());
    }

    @Override
    public String toString() {
        final StringBuilder stringBuilder = new StringBuilder(Integer.toString(getIdentifier()));
        for (final RollingStock rollingStock : rollingStocks) {
            stringBuilder.append(ROLLING_STOCK_SEPARATOR);
            stringBuilder.append(rollingStock.getIdentifier());
        }
        return stringBuilder.toString();
    }

    /**
     * Compare two trains based on their identifier.
     *
     * @param other train to compare to
     * @return integer comparison of the identifiers
     */
    @Override
    public int compareTo(Train other) {
        return Integer.compare(this.identifier, other.identifier);
    }
}
