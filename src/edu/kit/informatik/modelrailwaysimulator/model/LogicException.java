package edu.kit.informatik.modelrailwaysimulator.model;

/**
 * Thrown on illogical input or impossible to fulfill input.
 *
 * @author Arne Keller
 * @version 1.0
 */
public class LogicException extends Exception {
    /**
     * Construct a new logic exception with a specific message.
     *
     * @param message the error message
     */
    public LogicException(String message) {
        super(message);
    }
}
