package edu.kit.informatik.modelrailwaysimulator.model;

/**
 * Train set.
 *
 * @author Arne Keller
 * @version 1.0
 */
public class TrainSet extends RollingStock {
    /**
     * ASCII art representation of a train set.
     */
    private static final String[] TRAIN_SET_TEXT = new String[]{
        "         ++         ",
        "         ||         ",
        "_________||_________",
        "|  ___ ___ ___ ___ |",
        "|  |_| |_| |_| |_| |",
        "|__________________|",
        "|__________________|",
        "   (O)        (O)   "
    };

    /**
     * Series (class) of this train set.
     */
    private final String series;
    /**
     * Name of this train set.
     */
    private final String name;

    /**
     * Construct a new train set.
     *
     * @param series series/class of train set
     * @param name name of train set
     * @param length length of train set
     * @param couplingFront whether the train set should have a front coupling
     * @param couplingBack whether the train set should have a back coupling
     * @throws LogicException on invalid user input (e.g. zero-sized train set)
     */
    public TrainSet(String series, String name, int length, boolean couplingFront, boolean couplingBack)
            throws LogicException {
        super(length, couplingFront, couplingBack);
        this.name = name;
        this.series = series;
    }

    @Override
    public String getIdentifier() {
        return String.format("%s-%s", series, name);
    }

    @Override
    public String toString() {
        return String.format("%s %s %d %b %b", series, name, getLength(), hasCouplingFront(), hasCouplingBack());
    }

    @Override
    public boolean canCoupleTo(RollingStock rollingStock) {
        return rollingStock.getClass().equals(this.getClass()) && ((TrainSet) rollingStock).series.equals(this.series);
    }

    @Override
    public boolean usefulAtEndOfTrain() {
        return true;
    }

    @Override
    public String[] textRepresentation() {
        // make sure caller can not modify private data
        return TRAIN_SET_TEXT.clone();
    }

    @Override
    public String description() {
        return "train-set";
    }
}
