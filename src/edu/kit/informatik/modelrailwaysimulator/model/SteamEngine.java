package edu.kit.informatik.modelrailwaysimulator.model;

/**
 * Steam engine.
 *
 * @author Arne Keller
 * @version 1.0
 */
public class SteamEngine extends Engine {
    /**
     * ASCII art representation of a steam engine.
     */
    private static final String[] STEAM_ENGINE_TEXT = new String[]{
        "     ++      +------",
        "     ||      |+-+ | ",
        "   /---------|| | | ",
        "  + ========  +-+ | ",
        " _|--/~\\------/~\\-+ ",
        "//// \\_/      \\_/   "
    };

    /**
     * Construct a new steam engine.
     *
     * @param series series/class of engine
     * @param name name of engine
     * @param length length of engine
     * @param couplingFront whether the engine should have a front coupling
     * @param couplingBack whether the engine should have a back coupling
     * @throws LogicException on invalid user input (e.g. zero-sized engine)
     */
    public SteamEngine(String series, String name, int length, boolean couplingFront, boolean couplingBack)
            throws LogicException {
        super(series, name, length, couplingFront, couplingBack);
    }

    @Override
    public String getType() {
        return "s";
    }

    @Override
    public String[] textRepresentation() {
        // make sure caller can not modify private data
        return STEAM_ENGINE_TEXT.clone();
    }

    @Override
    public String description() {
        return "steam engine";
    }
}
