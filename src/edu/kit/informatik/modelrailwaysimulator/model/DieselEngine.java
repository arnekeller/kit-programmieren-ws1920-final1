package edu.kit.informatik.modelrailwaysimulator.model;

/**
 * Diesel engine.
 *
 * @author Arne Keller
 * @version 1.0
 */
public class DieselEngine extends Engine {
    /**
     * ASCII art representation of a diesel engine.
     */
    private static final String[] DIESEL_ENGINE_TEXT = new String[]{
        "  _____________|____  ",
        " /_| ____________ |_\\ ",
        "/   |____________|   \\",
        "\\                    /",
        " \\__________________/ ",
        "  (O)(O)      (O)(O)  ",
    };

    /**
     * Construct a new diesel engine.
     *
     * @param series series/class of engine
     * @param name name of engine
     * @param length length of engine
     * @param couplingFront whether the engine should have a front coupling
     * @param couplingBack whether the engine should have a back coupling
     * @throws LogicException on invalid user input (e.g. zero-sized engine)
     */
    public DieselEngine(String series, String name, int length,
                        boolean couplingFront, boolean couplingBack) throws LogicException {
        super(series, name, length, couplingFront, couplingBack);
    }

    @Override
    public String getType() {
        return "d";
    }

    @Override
    public String[] textRepresentation() {
        // make sure caller can not modify private data
        return DIESEL_ENGINE_TEXT.clone();
    }

    @Override
    public String description() {
        return "diesel engine";
    }
}
