package edu.kit.informatik.modelrailwaysimulator.model;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Queue;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * A rail network consisting of tracks and switches. Is only concerned with rails, does not process trains.
 *
 * @author Arne Keller
 * @version 1.0
 */
public class RailwayNetwork {
    /**
     * Rail in this railway network. Usually contains tracks and switches.
     */
    private final Map<Integer, Rail> rails = new HashMap<>();

    /**
     * Add a new track to the rail network.
     *
     * @param start start position of the new track
     * @param end end position of the new track
     * @return the positive identifier of the new track if successful, -1 if none available
     * @throws LogicException if the user provides incorrect input
     */
    public int addTrack(Vector2D start, Vector2D end) throws LogicException {
        final long startPossibleConnections = rails.values().stream().filter(rail -> rail.canConnectTo(start)).count();
        final long endPossibleConnections = rails.values().stream().filter(rail -> rail.canConnectTo(end)).count();
        if (startPossibleConnections == 2 || endPossibleConnections == 2) {
            throw new LogicException("track would connect to two other rails");
        }
        if (rails.isEmpty()) {
            final Track newTrack = new Track(start, end, 1);
            rails.put(1, newTrack);
            return newTrack.getIdentifier();
        } else {
            if (rails.values().stream().anyMatch(rail -> rail.canConnectTo(start) || rail.canConnectTo(end))) {
                final int id = getNextRailIdentifier();
                if (id < 0) {
                    return -1;
                }
                final Track newTrack = new Track(start, end, id);
                rails.put(id, newTrack);
                return newTrack.getIdentifier();
            } else {
                throw new LogicException("track is not connected to other tracks");
            }
        }
    }

    /**
     * Add a switch to the rail network.
     *
     * @param start start point of the switch
     * @param end1 end position 1 of the switch
     * @param end2 end position 2 of the switch
     * @return the positive identifier of the switch if successful, -1 otherwise
     * @throws LogicException when the new switch would be invalid
     */
    public int addSwitch(Vector2D start, Vector2D end1, Vector2D end2) throws LogicException {
        if (rails.isEmpty()) {
            final Switch newSwitch = new Switch(start, end1, end2, 1);
            rails.put(1, newSwitch);
            return newSwitch.getIdentifier();
        } else {
            final long startConnections = rails.values().stream().filter(rail -> rail.canConnectTo(start)).count();
            final long end1Connections = rails.values().stream().filter(rail -> rail.canConnectTo(end1)).count();
            final long end2Connections = rails.values().stream().filter(rail -> rail.canConnectTo(end2)).count();
            if (startConnections == 2 || end1Connections == 2 || end2Connections == 2) {
                throw new LogicException("switch endpoint would connect to two other rails");
            }
            if (rails.values().stream()
                    .anyMatch(rail -> rail.canConnectTo(start) || rail.canConnectTo(end1) || rail.canConnectTo(end2))) {
                final int id = getNextRailIdentifier();
                if (id < 0) {
                    return -1;
                }
                final Switch newSwitch = new Switch(start, end1, end2, id);
                rails.put(id, newSwitch);
                return newSwitch.getIdentifier();
            } else {
                throw new LogicException("switch not connected to other rails");
            }
        }
    }

    /**
     * Get the rail with the specified identifier.
     *
     * @param id rail identifier to get
     * @return the rail
     */
    public Optional<Rail> getRail(int id) {
        return Optional.ofNullable(rails.get(id));
    }

    /**
     * Remove the specified rail from the rail network.
     *
     * @param id identifier of the rail to remove
     * @return whether the rail could be successfully removed
     */
    public boolean removeRail(int id) {
        final Rail toRemove = rails.get(id);
        if (toRemove == null) {
            return false;
        }
        // locate one connected rail
        final Optional<Rail> otherRail = rails.values().stream()
                .filter(rail -> rail.getIdentifier() != toRemove.getIdentifier() && rail.canConnectToRail(toRemove))
                .findFirst();
        if (!otherRail.isPresent()) {
            rails.clear(); // last rail left, can always remove
            return true;
        }
        // check that rails would still be connected after removing this rail
        final Set<Rail> connectedRails = new HashSet<>();
        connectedRails.add(otherRail.get());
        final Queue<Rail> toProcess = new LinkedList<>();
        toProcess.add(otherRail.get());
        // basically breadth-first search (using a queue)
        while (!toProcess.isEmpty()) {
            final Rail connected = toProcess.poll();

            for (final Rail other : rails.values()) {
                if (other != toRemove && !connectedRails.contains(other) && other.canConnectToRail(connected)) {
                    connectedRails.add(other);
                    toProcess.offer(other);
                }
            }
        }
        return connectedRails.size() == rails.size() - 1 && rails.remove(id) != null;
    }

    /**
     * Change the setting of a switch in the rail network.
     *
     * @param id identifier of the switch
     * @param position position to set the switch to
     * @return whether the switch could be set
     */
    public boolean setSwitch(int id, Vector2D position) {
        final Rail toSwitch = rails.get(id);
        return toSwitch != null && toSwitch.switchTo(position);
    }

    /**
     * Get a sorted list of all rails in the network, specifying their type, identifier, start and end points and
     * their length.
     *
     * @return list of rails
     */
    public List<String> listTracks() {
        return rails.keySet().stream().sorted().map(rails::get).map(Object::toString).collect(Collectors.toList());
    }

    /**
     * @return the next positive rail identifier, or -1 if none available
     */
    private int getNextRailIdentifier() {
        return IntStream.rangeClosed(1, Integer.MAX_VALUE)
                .filter(id -> !rails.containsKey(id))
                .findFirst().orElse(-1);
    }

    /**
     * Move a position along the rails of this rail network in the specified direction.
     *
     * @param position the position to move
     * @param direction has to be (0,1) (0,-1) (1,0) or (-1,0)
     * @param steps amount of steps to go
     * @return next position, null if off the rails after single step
     */
    public Vector2D move(Vector2D position, Vector2D direction, long steps) {
        final Optional<Rail> containingRail = findContainingRail(position);
        if (containingRail.isPresent()) { // if position is on a rail, simply move along that rail
            return containingRail.get().move(position, direction, steps);
        }
        final Rail[] touchingRails = findTouchingRails(position);
        if (touchingRails.length == 0) {
            return null; // there is no rail to move on
        } else if (touchingRails.length == 1) { // simply move along the rail
            return touchingRails[0].move(position, direction, 1);
        } // else: check movement along both rails and pick the correct rail
        final Vector2D onRailOne = touchingRails[0].move(position, direction, steps);
        final Vector2D onRailTwo = touchingRails[1].move(position, direction, steps);
        // if we are stuck on the first rail
        //   or derail on the first rail
        //   or move in the correct direction on the second rail
        if (position.equals(onRailOne) || onRailOne == null
                || onRailTwo != null && onRailTwo.subtract(position).directionEquals(direction)) {
            return onRailTwo; // return movement on rail two
        } else if (position.equals(onRailTwo) || onRailTwo == null // see above, same logic for the other rail
                || onRailOne.subtract(position).directionEquals(direction)) {
            return onRailOne;
        } else {
            // this case: https://ilias.studium.kit.edu/goto.php?client_id=produktiv&target=frm_996924_139074_534267
            // it will not be tested, but simply refusing to place a train
            // if there is more than one way to place it is fine
            return null;
        }
    }

    /**
     * Check whether a train can be placed at the specified position in the specified direction.
     *
     * @param position front position of the train
     * @param direction initial direction of the train
     * @return whether placement is possible
     */
    public boolean allowsPlacementAt(Vector2D position, Vector2D direction) {
        // check that the requested direction is equal to the direction of one the tracks
        final Rail[] touchingRails = findTouchingRails(position);
        if (touchingRails.length == 0) {
            return findContainingRail(position)
                    .map(rail -> rail.allowsPlacement(position, direction))
                    .orElse(false); // containing rail is orthogonal to the requested direction
        } else if (touchingRails.length == 1) {
            // rail should not be orthogonal to the requested direction
            return touchingRails[0].allowsPlacement(position, direction)
                    || touchingRails[0].allowsMovement(position, direction);
        } else if (!touchingRails[0].allowsPlacement(position, direction) // train can not be placed on either rail
                && !touchingRails[1].allowsPlacement(position, direction) //  in the specified direction
                && !(touchingRails[0].allowsMovement(position, direction) // *and* it can not move in the specified
                || touchingRails[1].allowsMovement(position, direction))) { // direction afterwards
            // rail network does not allow this direction according to
            // https://ilias.studium.kit.edu/goto.php?client_id=produktiv&target=frm_996924_139143_534378
            return false;
        }
        return true;
    }

    /**
     * Find a rail that contains the specified position.
     *
     * @param position the position to check
     * @return the rail that contains the position
     */
    public Optional<Rail> findContainingRail(Vector2D position) {
        return rails.values().stream().filter(rail -> rail.contains(position)).findFirst();
    }

    /**
     * Find all rails that *touch* (not contain) this position.
     *
     * @param position the position to check
     * @return the rail(s) that touch this position
     */
    public Rail[] findTouchingRails(Vector2D position) {
        return rails.values().stream().filter(rail -> rail.canConnectTo(position)).toArray(Rail[]::new);
    }

    /**
     * Find a rail that connects to or contains the first position and connects to or contains the second position.
     *
     * @param positionA point which the rail should touch or connect to
     * @param positionB point which the rail should touch or connect to
     * @return rail satisfying these conditions
     */
    public Optional<Rail> findRail(Vector2D positionA, Vector2D positionB) {
        return rails.values().stream()
                .filter(rail -> rail.connectsTo(positionA) || rail.contains(positionA))
                .filter(rail -> rail.connectsTo(positionB) || rail.contains(positionB))
                .findFirst();
    }

    /**
     * Check whether the rail network is ready for trains (all switches set, etc.).
     *
     * @return whether the rail network is ready
     */
    public boolean isReadyForTrains() {
        return rails.values().stream().allMatch(Rail::isReadyForTrains);
    }
}
