package edu.kit.informatik.modelrailwaysimulator;

import edu.kit.informatik.modelrailwaysimulator.ui.CommandLine;

/**
 * The main class.
 *
 * @author Arne Keller
 * @version 1.0
 */
public final class Main {
    /**
     * Utility class: private constructor.
     */
    private Main() {

    }

    /**
     * Program entry point.
     *
     * @param args command-line arguments
     */
    public static void main(String[] args) {
        CommandLine.startInteractive();
    }
}
