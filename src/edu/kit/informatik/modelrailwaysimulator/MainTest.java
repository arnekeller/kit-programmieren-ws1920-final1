package edu.kit.informatik.modelrailwaysimulator;

import edu.kit.informatik.Terminal;
import org.junit.jupiter.api.Test;

import java.io.*;
import java.nio.file.Files;

import static org.junit.jupiter.api.Assertions.*;

@SuppressWarnings("ALL")
class MainTest {
    @Test
    void basics() throws IOException {
        cmpInOut("basics_input.txt", "basics_output.txt");
    }

    @Test
    void exampleInteraction() throws IOException {
        cmpInOut("example1input.txt", "example1ONLYoutput.txt");
    }

    @Test
    void limits() throws IOException {
        cmpInOut("example2input.txt", "example2output.txt");
    }

    @Test
    void loopy() throws IOException {
        cmpInOut("loopyinput.txt", "loopyoutput.txt");
    }

    @Test
    void hitMeBabyOneMoreTime() throws IOException {
        cmpInOut("stophittingyourself_input.txt", "stophittingyourself_output.txt");
    }

    @Test
    void fuzz1() throws IOException {
        cmpInOut("fuzz1_input.txt", "fuzz1_output.txt");
    }

    @Test
    void fuzz2() throws IOException {
        cmpInOut("fuzz2_input.txt", "fuzz2_output.txt");
    }

    @Test
    void fuzz3() throws IOException {
        cmpInOut("fuzz3_input.txt", "fuzz3_output.txt");
    }

    @Test
    void fuzz4() throws IOException {
        cmpInOut("fuzz4_input.txt", "fuzz4_output.txt");
    }

    @Test
    void fuzz5() throws IOException {
        cmpInOut("fuzz5_input.txt", "fuzz5_output.txt");
    }

    @Test
    void fuzz6() throws IOException {
        cmpInOut("fuzz6_input.txt", "fuzz6_output.txt");
    }

    @Test
    void fuzz7() throws IOException {
        cmpInOut("fuzz7_input.txt", "fuzz7_output.txt");
    }

    @Test
    void fuzz8() throws IOException {
        cmpInOut("fuzz8_input.txt", "fuzz8_output.txt");
    }

    @Test
    void fuzz9() throws IOException {
        cmpInOut("fuzz9_input.txt", "fuzz9_output.txt");
    }

    @Test
    void fuzz10() throws IOException {
        cmpInOut("fuzz10_input.txt", "fuzz10_output.txt");
    }

    @Test
    void fuzz11() throws IOException {
        cmpInOut("fuzz11_input.txt", "fuzz11_output.txt");
    }

    @Test
    void testCoverage() throws IOException {
        cmpInOut("testcoverage_input.txt", "testcoverage_output.txt");
    }

    @Test
    void oneLengthDerailEdgecase() throws IOException {
        cmpInOut("train_one_length_input.txt", "train_one_length_output.txt");
    }

    @Test
    void switchInconsistency() throws IOException {
        cmpInOut("switch_inconsistency_input.txt", "switch_inconsistency_output.txt");
    }
    
    @Test
    void largeGenericTest() throws IOException {
        cmpInOut("lgt_input.txt", "lgt_output.txt");
    }

    @Test
    void newestIliasShit() throws IOException {
        cmpInOut("newest_ilias_shit_input.txt", "newest_ilias_shit_output.txt");
    }

    @Test
    void longTrain() throws IOException {
        cmpInOut("long_train_input.txt", "long_train_output.txt");
    }

    @Test
    void testCoverage2() throws IOException {
        cmpInOut("testcoverage2_input.txt", "testcoverage2_output.txt");
    }

    @Test
    void spt() throws IOException {
        cmpInOut("spt_input.txt", "spt_output.txt");
    }

    @Test
    void fuzz12() throws IOException {
        cmpInOut("fuzz12_input.txt", "fuzz12_output.txt");
    }

    @Test
    void test13() throws IOException {
        cmpInOut("test13_input.txt", "test13_output.txt");
    }

    @Test
    void switchDerailsAllTrains() throws IOException {
        cmpInOut("switch_derails_all_trains_input.txt", "switch_derails_all_trains_output.txt");
    }

    private void cmpInOut(String in, String out) throws IOException {
        System.setIn(new ByteArrayInputStream(readFile(in)));
        ByteArrayOutputStream output = new ByteArrayOutputStream();
        System.setOut(new PrintStream(output));
        Terminal.reload();
        Main.main(null);
        String expected = new String(readFile(out));
        assertEquals(expected, output.toString());
    }

    private byte[] readFile(String name) throws IOException {
        File file = new File(name);
        return Files.readAllBytes(file.toPath());
    }
}