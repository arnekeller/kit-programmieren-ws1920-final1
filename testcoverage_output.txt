No coach exists
No train exists
No train-set exists
No engine exists
T2-Phi
T2-Rho
T2-Omega
diesel engine T2-Phi added to train 1
steam engine T2-Rho added to train 1
electrical engine T2-Omega added to train 1
1
2
3
passenger coach W1 added to train 1
freight coach W2 added to train 1
special coach W3 added to train 1
                                                           ___                                                                  
                                                             \                                                              ____
  _____________|____        ++      +------   _______________/__   ____________________                      /--------------|  |
 /_| ____________ |_\       ||      |+-+ |   /_| ____________ |_\  |  ___ ___ ___ ___ | |                  | \--------------|  |
/   |____________|   \    /---------|| | |  /   |____________|   \ |  |_| |_| |_| |_| | |                  |   | |          |  |
\                    /   + ========  +-+ |  \                    / |__________________| |                  |  _|_|__________|  |
 \__________________/   _|--/~\------/~\-+   \__________________/  |__________________| |__________________| |_________________|
  (O)(O)      (O)(O)   //// \_/      \_/      (O)(O)      (O)(O)      (O)        (O)       (O)        (O)       (O)       (O)   
Error, no such train
Error, train not found
4
passenger coach W4 added to train 2
Error, train is not valid
OK
OK
1
OK
Error, rolling stock length has to be positive
T2-Gamma
diesel engine T2-Gamma added to train 2
OK
2
Error, could not delete rail segment
Crash of train 2
OK
Error, engine identifier already used
T2-Epsilon
Error, engine identifier already used
Error, train set identifier already used
Error, train set identifier already used
T2-Lambda
train-set T2-Lambda added to train 3
         ++         
         ||         
_________||_________
|  ___ ___ ___ ___ |
|  |_| |_| |_| |_| |
|__________________|
|__________________|
   (O)        (O)   
Error, new train identifier must be next free identifier
1 T2-Phi T2-Rho T2-Omega W1 W2 W3
2 T2-Gamma
3 T2-Lambda
4
5
OK
4
OK
1 1 p 1 true true
2 1 f 1 true true
3 1 s 1 true true
4 none p 1 true true
none T2 Epsilon 1 true false
3 T2 Lambda 1 true true
2 d T2 Gamma 1 true true
1 e T2 Omega 2 true true
1 d T2 Phi 1 true true
1 s T2 Rho 3 true true
OK
electrical engine T2-Omega added to train 1
passenger coach W1 added to train 4
diesel engine T2-Phi added to train 5
freight coach W2 added to train 6
OK
OK
OK
steam engine T2-Rho added to train 2
2
OK
Crash of train 1
OK
OK
1
2
3
OK
OK
OK
Crash of train 1,2,5
4
5
T1-Albert
T1-Berta
train-set T1-Albert added to train 3
train-set T1-Berta added to train 4
OK
OK
OK
OK
Crash of train 2
Crash of train 3,4,5
Error, invalid track segment: not a straight line
Error, invalid track segment: not a straight line
Error, invalid add switch argument syntax
Error, invalid/missing delete track argument
Error, too many arguments for list tracks
Error, invalid create engine argument syntax
Error, too many list engines arguments
Error, invalid create coach arguments
Error, too many list coaches arguments
Error, invalid create train-set arguments
Error, invalid train-set class/series
Error, too many list train-sets arguments
Error, invalid delete rolling stock argument
Error, invalid add train arguments
Error, invalid delete train argument
Error, too many list trains arguments
Error, invalid show train argument
Error, invalid put train arguments
Error, invalid train direction
Error, invalid add track argument syntax
Error, unknown command
Error, invalid step argument
Error, input after exit command
OK
OK
OK
OK
OK
1
2
3
4
5
Error, could not set switch
OK
OK
OK
Crash of train 5
OK
OK
OK
Error, could not delete rail segment
OK
Error, could not delete rail segment
OK
1
2
3
OK
Crash of train 5
Error, track has length zero
Error, track would connect to two other rails
Error, track would connect to two other rails
T4-Einstein
T4-Newton
train-set T4-Einstein added to train 7
train-set T4-Newton added to train 7
T5-Hartnick
diesel engine T5-Hartnick added to train 6
OK
Crash of train 6
OK
diesel engine T5-Hartnick added to train 6
OK
Crash of train 6
Error, switch endpoint would connect to two other rails
Error, switch endpoint would connect to two other rails
Error, switch endpoint would connect to two other rails
OK
OK
OK
1
2
Error, switch endpoint would connect to two other rails
Error, switch endpoint would connect to two other rails
Error, switch endpoint would connect to two other rails
Error, switch not connected to other rails
OK
OK
1
2
3
4
T5-Worsch
steam engine T5-Worsch added to train 8
Error, could not place train
T5-Kühnlein
electrical engine T5-Kühnlein added to train 9
OK
OK
OK
OK
1
2
OK
OK
Crash of train 8,9
T6-aAᴲƻǈ
Error, could not set switch
